%% Experimento maxmin eval
% Set 3, subconjunto de evaluaci�n(b8020), recortado, desdistorsionado
% y con m�scaras binarias (segmentador bobo). Etiquetas: Primera,
% NoAlmendra, Mala.
%
% Se han ajustado las toleranciasde cada clasificador con los m�ximos y los
% m�nimos de lo que midieron sobre el conjunto de entrenamiento.


clear variables

run('../../inicializar.m');

%%
% Nombre experimento
experimento = 'maxmin';

carpeta_experimento = fullfile(carpeta_experimentos, experimento);
cd(carpeta_experimento);

%% EXPERIMENTO
tic
% Opcional: archivo de par�metros
archivo_parametros = 'parametros.yml';

% SET: lista de im�genes
set_dir = fullfile(carpeta_imagenes, 'set3\sindist_rec_bin\set3_b_8020_eval.csv');
set_nombre = 'set3_b_8020_eval';

%
PS = PrincipalSet(set_nombre, set_dir);

% Algoritmo:
PS.agregar(PreprocesadorBlur('Blur gaussiano'));
% PS.agregar( SegmentadorBorde1('Segmentador por borde') );
PS.agregar(SegmentadorBobo('Segmentador bobo', PS.datos));
PS.agregar(ClasificadorAltura('altura'));
PS.agregar(ClasificadorExtensionBB('extent'));
PS.agregar(ClasificadorElipseRA('relaci�n de aspecto de elipse'));
% PS.agregar(ClasificadorCajaRA('relaci�n de aspecto de caja'));
PS.agregar(ClasificadorRaspaduras2('�rea de raspaduras'));
PS.agregar(ClasificadorHueMedia('media de color'));
PS.agregar(ClasificadorHueDesviacion('desviaci�n est�ndar de color '));
PS.agregar(ClasificadorCIELabMedia('medias a y b CIELab'));
PS.agregar(ClasificadorSolidez('solidez'));
% PS.agregar( ClasificadorArrugas('arrugas') );

% Carga par�metros
cargarParametros(archivo_parametros, PS);

% Guardar par�metros
guardarParametros('parametros_efectivos_eval.yml', PS);

% PS.procesarTodo(forzar evaluaci�n)
R = PS.procesarTodo(true);

% guardar mediciones
guardarMediciones([experimento, '_eval_mediciones.xls'], PS, R);

% guardar evaluaci�n
PS.evaluar(R);
PS.guardarTablas([experimento, '_eval_evaluacion.xls']);

% guardar .m con datos
% save( [experimento,'_datos.m'] , 'PS', 'R');

% guardar imagenes segmentadas
% guardarSegmentacion(R,'./temp');

tt = toc;

minutos_totales = toc / 60
