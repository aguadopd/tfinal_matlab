%% Experimento con Balu
% Usamos la herramienta Balu para extraer muchos descriptores. Luego los
% usaremos en ClassificationLearner.
% Van descriptores para los canales H,S,V,R,G,B e Intensidad. Adem�s se
% calculan descriptores geom�tricos sobre la m�scara.
% 
% Por simplicidad, s�lo se calcularan los descriptores para la imagen del
% frente del objeto. Set3 completo, recortado y desdistorsionado. M�scaras
% binarias.
% 
% BALU: (c) D.Mery, PUC-DCC, 2011
% http://dmery.ing.puc.cl


clear variables

run('../../inicializar.m');
%% 
% Nombre experimento
experimento = 'balu';
archivo = [experimento,'_features.xls'];

carpeta_experimento = fullfile(carpeta_experimentos, experimento);
cd(carpeta_experimento);

%% Carga de archivos
% Archivo lista
dir_archivo_set = fullfile(carpeta_imagenes,'\set3\sindist_rec_bin\set3.csv');
% Carga de direcciones
[pathstr,~,~] = fileparts(dir_archivo_set);

%
% f_archivo_original; f_archivo_mascara; d_archivo_original; d_archivo_mascara; clase
formato = '%s%s%s%s%s';
delimitador = ';';
T = readtable( dir_archivo_set, 'Delimiter', delimitador, ...
    'Format', formato);

fao = table2cell(T(:,'f_archivo_original'));
fam = table2cell(T(:,'f_archivo_mascara'));
dao = table2cell(T(:,'d_archivo_original'));
dam = table2cell(T(:,'d_archivo_mascara'));
clas = table2cell(T(:,'clase'));


datos = cat(2,fao,fam,dao,dam,clas);


% Completado de direcciones Y copia de frente si no hay dorso
for i = 1 : size(datos,1)
    for j = 1 : 4
        if ~isempty( datos{i,j} )
            datos{i,j} = fullfile(pathstr, datos{i,j} );
        end
    end
    % Copia del frente si no hay dorso
    if isempty( datos{i,3} )
        datos(i,3:4) = datos(i,1:2);
    end
end

%% Configuraci�n de Balu
% Opciones para extracci�n de descriptores de intensidad
%   RGB
opi_rgb.b = Bfx_build({'basicint','huint'});
opi_rgb.colstr = 'rgb';
%   HSV
opi_hsv.b = Bfx_build({'basicint','huint'});
opi_hsv.colstr = 'hsv';
%   Gris / intensidad
opi_gris.b = Bfx_build({'basicint','huint'});                 % gris
opi_gris.colstr = 'i';

% Opciones para extracci�n de descriptores geom�tricos sobre la m�scara
opg.b = Bfx_build({'basicgeo','fitellipse','hugeo'});


%% Loop
tic

% Cantidad de im�genes
n =  size(datos,1);
% n = 5;

for i = 1 : n
    rgb = imread(datos{i,1});
    hsv = rgb2hsv(rgb);
    gris = rgb2gray(rgb);
    mascara = imread(datos{i,2});
    [X_geo,Xn_geo] = Bfx_geo(mascara,opg);
    [X_rgb,Xn_rgb] = Bfx_int(rgb,mascara,opi_rgb); 
    [X_hsv,Xn_hsv] = Bfx_int(hsv,mascara,opi_hsv); 
    [X_gris,Xn_gris] = Bfx_int(gris,mascara,opi_gris); 

    if i == 1
        f_geo = zeros(n,length(X_geo));
        f_rgb = zeros(n,length(X_rgb));
        f_hsv = zeros(n,length(X_hsv));
        f_gris = zeros(n,length(X_gris));
        
        f_geo(i,:) = X_geo;
        f_rgb(i,:) = X_rgb;
        f_hsv(i,:) = X_hsv;
        f_gris(i,:) = X_gris;
    else
        f_geo(i,:) = X_geo;
        f_rgb(i,:) = X_rgb;
        f_hsv(i,:) = X_hsv;
        f_gris(i,:) = X_gris;
    end
    disp(i);
end

%% Creaci�n de tablas

T_geo = array2table(f_geo);
c = cell(0);
for j = 1:size(Xn_geo,1);
    c(j) = {matlab.lang.makeValidName(Xn_geo(j,:))};
end
T_geo.Properties.VariableNames = c;


T_rgb = array2table(f_rgb);
c = cell(0);
for j = 1:size(Xn_rgb,1);
    c(j) = {matlab.lang.makeValidName(Xn_rgb(j,:))};
end
T_rgb.Properties.VariableNames = c;


T_hsv = array2table(f_hsv);
c = cell(0);
for j = 1:size(Xn_hsv,1);
    c(j) = {matlab.lang.makeValidName(Xn_hsv(j,:))};
end
T_hsv.Properties.VariableNames = c;


T_gris = array2table(f_gris);
c = cell(0);
for j = 1:size(Xn_gris,1);
    c(j) = {matlab.lang.makeValidName(Xn_gris(j,:))};
end
T_gris.Properties.VariableNames = c;


% CAT
T_clas = table(clas(1:n),'VariableNames',{'Clase_real'});

T_F = [T_clas, T_geo, T_rgb, T_hsv, T_gris];



% Se borra, si existe. No queremos crear hojas nuevas, por ejemplo.
delete(archivo);

% Escritura
writetable(T_F, archivo);



minutos_totales = toc / 60