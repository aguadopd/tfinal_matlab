% Genera m�scara a partir de imagen de retroiluminaci�n, la segmenta y
% recorta a partir del mayor objeto encontrado. Guarda imagen de
% retroiluminaci�n e imagen a color, recortadas. Necesita de una funci�n de
% segmentaci�n para poder encontrar un objeto.
%% Pre

% Archivo lista
dir_archivo_set = 'D:\GDRIVE\ya\tfinal\imagenes\set3\sindistorsion\set3.csv';

% Carpeta destino, en donde se guardar�n las im�genes y m�scaras
% recortadas.
carpeta_destino = 'D:\GDRIVE\ya\tfinal\imagenes\set3\sindist_rec\';
mkdir(carpeta_destino);

% Funci�n creadora de m�scaras.
funcion_segmentacion = @segmentarRetroiluminacion;

% Padding extra en p�xeles
padding_cada_lado = 5;

%% Carga de direcciones
[pathstr,~,~] = fileparts(dir_archivo_set);

%
% f_archivo_original; f_archivo_mascara; d_archivo_original; d_archivo_mascara; clase
formato = '%s%s%s%s%s';
delimitador = ';';
T = readtable( dir_archivo_set, 'Delimiter', delimitador, ...
    'Format', formato);

fao = table2cell(T(:,'f_archivo_original'));
fam = table2cell(T(:,'f_archivo_mascara'));
dao = table2cell(T(:,'d_archivo_original'));
dam = table2cell(T(:,'d_archivo_mascara'));
c = table2cell(T(:,'clase'));


datos = cat(2,fao,fam,dao,dam,c);


% Completado de direcciones Y copia de frente si no hay dorso
for i = 1 : size(datos,1)
    for j = 1 : 4
        if ~isempty( datos{i,j} )
            datos{i,j} = fullfile(pathstr, datos{i,j} );
        end
    end
    % Copia del frente si no hay dorso
    if isempty( datos{i,3} )
        datos(i,3:4) = datos(i,1:2);
    end
end
%%

% Cantidad de objetos.
n_objetos = size(datos,1);

for i = 1:n_objetos
   % Carga
    imagen_frente = imread(datos{i,1});
    backlight_frente = imread(datos{i,2});
    imagen_dorso = imread(datos{i,3});
    backlight_dorso = imread(datos{i,4});
    
    % M�scara a partir de backlight
    mascara_frente = funcion_segmentacion(backlight_frente);
    mascara_dorso = funcion_segmentacion(backlight_dorso);
    
    % Recorte
    propiedades_frente = regionprops(mascara_frente, 'BoundingBox');
    propiedades_dorso = regionprops(mascara_dorso, 'BoundingBox');
    
    bb_frente = propiedades_frente(1).BoundingBox;
    bb_frente(1) = bb_frente(1) - padding_cada_lado;
    bb_frente(2) = bb_frente(2) - padding_cada_lado;
    bb_frente(3) = bb_frente(3) + 2*padding_cada_lado;
    bb_frente(4) = bb_frente(4) + 2*padding_cada_lado;
    imagen_frente_recortada = imcrop(imagen_frente, bb_frente);
    backlight_frente_recortada = imcrop(backlight_frente, bb_frente);
    
    bb_dorso = propiedades_dorso(1).BoundingBox;
    bb_dorso(1) = bb_dorso(1) - padding_cada_lado;
    bb_dorso(2) = bb_dorso(2) - padding_cada_lado;
    bb_dorso(3) = bb_dorso(3) + 2*padding_cada_lado;
    bb_dorso(4) = bb_dorso(4) + 2*padding_cada_lado;
    imagen_dorso_recortada = imcrop(imagen_dorso, bb_dorso);
    backlight_dorso_recortada = imcrop(backlight_dorso, bb_dorso);
    
    % Escritura
    imwrite(imagen_frente_recortada, fullfile(carpeta_destino,fao{i}));
    imwrite(backlight_frente_recortada, fullfile(carpeta_destino,fam{i}));
    imwrite(imagen_dorso_recortada, fullfile(carpeta_destino,dao{i}));
    imwrite(backlight_dorso_recortada, fullfile(carpeta_destino,dam{i}));
        
end