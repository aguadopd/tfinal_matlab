addpath('../clases');
addpath('../funciones');
%addpath('../../../imagenes/set1/');

% Se crea objeto Principal Set
PS = PrincipalSet('set2', '..\..\..\imagenes\set2\set2.csv');

% Lo cargamos con preprocesador, segmentador y clasificadores
PS.agregar( PreprocesadorBlur('Blur gaussiano') );
% PS.agregar( SegmentadorHue1('Segmentador por matiz') );
PS.agregar( SegmentadorBorde1('Segmentador por borde') );
PS.agregar( ClasificadorAltura('Clasificador por altura') );
% PS.agregar( ClasificadorHueMedia('Clasificador por media de color') );
PS.agregar( ClasificadorExtensionBB('Clasificador por extent') );
PS.agregar( ClasificadorElipseRA('Clasificador por relaci�n de aspecto de elipse') );
PS.agregar( ClasificadorRaspaduras('Clasificador por �rea de raspaduras') );
PS.agregar( ClasificadorHueDesviacion('Clasificador por desviaci�n est�ndar de color ') );
PS.agregar( ClasificadorArrugas('Clasificador por arrugas') );

% Procesa
r = PS.procesarTodo(true);

% Resultados
PS.evaluar(r);

%%
% mostrarSegmentacion(r);
% guardarSegmentacion(r,'./temp');