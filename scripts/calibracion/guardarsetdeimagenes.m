%% La configuraci�n usada est� en config.iat
% vid = videoinput('winvideo', 1, 'YUY2_1600x1200');
% src = getselectedsource(vid);% 
% vid.FramesPerTrigger = 1;% 
% preview(vid);% 
% vid.ReturnedColorspace = 'rgb';% 
% src.BacklightCompensation = 'off';% 
% src.FocusMode = 'manual';% 
% triggerconfig(vid, 'manual');% 
% src.Saturation = 86;% 
% src.Brightness = 20;% 
% vid.TriggerRepeat = Inf;


%% Guardado
% vvv es el arreglo exportado desde imaqtool
arreglo = vvv;
carpeta_destino = '../../../../imagenes/calibracion';
mkdir(carpeta_destino);

% N�mero de inicio de los nombres de las im�genes guardadas
numero_inicio = 1;

j = 1; % aux
for i = 1 : 4 : size(arreglo,4)    
    imwrite(arreglo(:,:,:,i), [carpeta_destino, '/', sprintf('%03d',j+numero_inicio-1),'f','.png']);
    imwrite(arreglo(:,:,:,i+1), [carpeta_destino, '/', sprintf('%03d',j+numero_inicio-1),'f_m','.png']);
    imwrite(arreglo(:,:,:,i+2), [carpeta_destino, '/', sprintf('%03d',j+numero_inicio-1),'r_m','.png']);
    imwrite(arreglo(:,:,:,i+3), [carpeta_destino, '/', sprintf('%03d',j+numero_inicio-1),'r','.png']);
    j = j+1;
end