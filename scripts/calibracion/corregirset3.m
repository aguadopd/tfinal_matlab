%% Pre

% Archivo lista
dir_archivo_set = 'D:\GDRIVE\ya\tfinal\imagenes\set3\set3.csv';

% Carpeta destino, en donde se guardar�n las im�genes y m�scaras
% recortadas.
carpeta_destino = 'D:\GDRIVE\ya\tfinal\imagenes\set3\sindistorsion\';
mkdir(carpeta_destino);

% Carga de par�metros de la c�mara, previamente calculados.
% La variables es cameraParams
load parametros.mat

%% Carga de direcciones
[pathstr,~,~] = fileparts(dir_archivo_set);

%
% f_archivo_original; f_archivo_mascara; d_archivo_original; d_archivo_mascara; clase
formato = '%s%s%s%s%s';
delimitador = ';';
T = readtable( dir_archivo_set, 'Delimiter', delimitador, ...
    'Format', formato);

fao = table2cell(T(:,'f_archivo_original'));
fam = table2cell(T(:,'f_archivo_mascara'));
dao = table2cell(T(:,'d_archivo_original'));
dam = table2cell(T(:,'d_archivo_mascara'));
c = table2cell(T(:,'clase'));


datos = cat(2,fao,fam,dao,dam,c);


% Completado de direcciones Y copia de frente si no hay dorso
for i = 1 : size(datos,1)
    for j = 1 : 4
        if ~isempty( datos{i,j} )
            datos{i,j} = fullfile(pathstr, datos{i,j} );
        end
    end
    % Copia del frente si no hay dorso
    if isempty( datos{i,3} )
        datos(i,3:4) = datos(i,1:2);
    end
end
%%

% Cantidad de objetos.
n_objetos = size(datos,1);

for i = 1:n_objetos
   % Carga
    imagen_frente = imread(datos{i,1});
    mascara_frente = imread(datos{i,2});
    imagen_dorso = imread(datos{i,3});
    mascara_dorso = imread(datos{i,4});
    
    % Correci�n de la distorsi�n
    imagen_frente_corregida = undistortImage(imagen_frente, cameraParams);
    mascara_frente_corregida = undistortImage(mascara_frente, cameraParams);
    imagen_dorso_corregida = undistortImage(imagen_dorso, cameraParams);
    mascara_dorso_corregida = undistortImage(mascara_dorso, cameraParams);
    
    % Escritura
    imwrite(imagen_frente_corregida, fullfile(carpeta_destino,fao{i}));
    imwrite(mascara_frente_corregida, fullfile(carpeta_destino,fam{i}));
    imwrite(imagen_dorso_corregida, fullfile(carpeta_destino,dao{i}));
    imwrite(mascara_dorso_corregida, fullfile(carpeta_destino,dam{i}));
        
end