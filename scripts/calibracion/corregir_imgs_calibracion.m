% Origen
carpeta_origen = 'D:\GDRIVE\ya\tfinal\imagenes\calibracion';
% extension = '.png';

% Destino
carpeta_destino = 'D:\GDRIVE\ya\tfinal\imagenes\calibracion\sindist';
mkdir(carpeta_destino);

% Carga de par�metros de la c�mara, previamente calculados.
% La variables es cameraParams
load parametros.mat

% Carga
f1 = dir(carpeta_origen);
f1 = f1([f1.isdir]==0);
f1(strncmp({f1.name},'.',1))=[]; %Not . and .. folders
for i = 1:length(f1);
    archivos{i} = fullfile(carpeta_origen,f1(i).name);
end

%% Correci�n
for i = 1:length(archivos)
    original = imread(archivos{i});
    sindistorsion = undistortImage(original, cameraParams);
    imwrite(sindistorsion, fullfile(carpeta_destino, f1(i).name));
end