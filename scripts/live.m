% Este script es para demostrar funcionamiento en l�nea conectado a una
% c�mara.

% Se crea el objeto c�mara
cam = webcam(1);

figure();


% Bucle de prueba
while(true)
    % Espera tecla
    pause();
    
    % Captura imagen
    img = snapshot(cam);
    
    % Evaluaci�n
    R = evaluar( img );
    
    % Show
    if R.hay_objeto == false
        continue
    end
    imshow(imfuse(R.imagen_original,R.mascara,'falsecolor'));
end

    
        
    