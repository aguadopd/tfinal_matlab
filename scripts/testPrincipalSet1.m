addpath('../clases');
addpath('../funciones');
%addpath('../../../imagenes/set1/');

% Se crea objeto Principal Set
PS = PrincipalSet('set1', '..\..\..\imagenes\set1\set1.csv');

% Lo cargamos con preprocesador, segmentador y clasificadores
PS.agregar( PreprocesadorBlur('Blur gaussiano') );
PS.agregar( SegmentadorHue1('Segmentador por matiz') );
% PS.agregar( SegmentadorBorde1('Segmentador por borde') );
PS.agregar( ClasificadorExtensionBB('Clasificador por extent') );

% Procesa
r = PS.procesarTodo();

% Resultados
PS.evaluar(r,1);

%%
% mostrarSegmentacion(r);
guardarSegmentacion(r,'./temp');