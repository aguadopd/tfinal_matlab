% carpeta_anterior = pwd;
carpeta_base = 'D:\GDRIVE\ya\tfinal'; % absoluta o relativa a la carpeta del experimento
carpeta_imagenes = fullfile(carpeta_base,'imagenes');
carpeta_experimentos = fullfile(carpeta_base,'experimentos');
carpeta_base_programas = fullfile(carpeta_base, 'desarrollo/tfinal_matlab');

addpath(fullfile(carpeta_base_programas,'scripts'));
addpath(fullfile(carpeta_base_programas,'clases'));
addpath(genpath(fullfile(carpeta_base_programas,'funciones')));
addpath(fullfile(carpeta_base_programas,'GUI'));

%%
% Supongo que este archivo est� en la carpeta del experimento.
experimento = '0001';
% carpeta_experimento = fullfile(carpeta_experimentos,'experimento');
% mkdir(carpeta_experimento);

%% EXPERIMENTO
% Opcional: archivo de par�metros
archivo_parametros = '';

% Algoritmo
algoritmo = {...
    PreprocesadorBlur('Blur gaussiano'),...
    SegmentadorBorde1('Segmentador por borde'),...
    ClasificadorExtensionBB('Clasificador por extent'),...
    };

% SET: lista de im�genes
set_dir = fullfile(carpeta_imagenes,'set1\set1.csv');
set_nombre = 'set1';
%subset = {'Primera','Mala'};

%
PS = PrincipalSet(set_nombre, set_dir); %,subset);

for i = 1:length(algoritmo)
    PS.agregar( algoritmo{i} );
end

% cargarParametros(archivo_parametros, PS);

R = PS.procesarTodo();

% guardar mediciones
guardarMediciones([experimento,'_mediciones.xls'], PS, R);

% guardar evaluaci�n
PS.evaluar(R);
PS.guardarTablas([experimento,'_evaluacion.xls']);

% guardar .m con datos
save( [experimento,'_datos.m'] , 'PS', 'R');

% guardar imagenes segmentadas
% guardarSegmentacion(R,'./temp');