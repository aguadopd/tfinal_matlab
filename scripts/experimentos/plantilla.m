clear variables
carpeta_anterior = pwd;
path_anterior = path;

carpeta_base = 'D:\GDRIVE\ya\tfinal'; % absoluta o relativa a la carpeta del experimento
carpeta_imagenes = fullfile(carpeta_base,'imagenes');
carpeta_experimentos = fullfile(carpeta_base,'experimentos');
carpeta_base_programas = fullfile(carpeta_base, 'desarrollo/tfinal_matlab');

addpath(fullfile(carpeta_base_programas,'scripts'));
addpath(fullfile(carpeta_base_programas,'clases'));
addpath(genpath(fullfile(carpeta_base_programas,'funciones')));
addpath(fullfile(carpeta_base_programas,'GUI'));


%%
% Nombre experimento
experimento = '0001';

carpeta_experimento = fullfile(carpeta_experimentos, experimento);
cd(carpeta_experimento);

%% EXPERIMENTO
% Opcional: archivo de par�metros
archivo_parametros = 'parametros.yml';

% SET: lista de im�genes
set_dir = fullfile(carpeta_imagenes,'set1\set1.csv');
set_nombre = 'set1';

%
PS = PrincipalSet(set_nombre, set_dir); 

% Algoritmo:
% Algoritmo
PS.agregar( PreprocesadorBlur('Blur gaussiano') );
PS.agregar( SegmentadorBorde1('Segmentador por borde') );
PS.agregar( SegmentadorHue1('Segmentador por matiz') );
PS.agregar( ClasificadorExtensionBB('Clasificador por extent') );

% Carga par�metros
cargarParametros(archivo_parametros, PS);

% PS.procesarTodo(forzar evaluaci�n)
R = PS.procesarTodo(true);

% guardar mediciones
guardarMediciones([experimento,'_mediciones.xls'], PS, R);

% guardar evaluaci�n
PS.evaluar(R);
PS.guardarTablas([experimento,'_evaluacion.xls']);

% guardar .m con datos
save( [experimento,'_datos.m'] , 'PS', 'R');

% guardar imagenes segmentadas
guardarSegmentacion(R,'./temp');


%%
% Se restablece el path
path(path_anterior);

cd(carpeta_anterior);

clear variables