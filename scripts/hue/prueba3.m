% h = [1, 15, 15, 30, 31, 31]; % 
% h = [341, 355, 355, 10, 11, 11]; % anterior -20�
h = [90, 90 , 91, 95, 98, 99, 120, 125, 345,345,345]; 

% ESTAD�STICA DIRECCIONAL o ESTAD�STICA CIRCULAR
% Conversi�n a coordenadas cartesianas, desde polares. Cada �ngulo
% representa un vector de longitud unitaria. Las componentes, entonces, son
% el coseno y el seno del �ngulo.
[cartesianas_cos, cartesianas_sin] = pol2cart(deg2rad(h),ones(1,length(h)));

% Se construye un vector R formado por la suma de cada uno de los �ngulos
% iniciales. El �ngulo de R es el �ngulo medio de los vectores, mientras
% que su m�dulo se utiliza para estimar una varianza.

% Sumas de componentes
suma_cos = sum(cartesianas_cos);
suma_sin = sum(cartesianas_sin);

% El �ngulo medio
angulo_medio = rad2deg(atan2(suma_sin,suma_cos));

% M�dulo y longitud media de los vectores, seg�n la longitud de R. Longitud
% media: [0, 1]
longitud_vector = sqrt(suma_cos^2 + suma_sin^2);
longitud_media = longitud_vector / length(h);

% Varianza [0, 1]
varianza = 1-longitud_media; % entre 0 y 1

% Desviaci�n est�ndar [0, inf]
desviacion = sqrt(-2*log(longitud_media)); % entre 0 y inf

% Conversi�n a grados
desviacion = desviacion*360/(2*pi);


angulo_medio
desviacion

% Verificaci�n con Circstat2012a
addpath('D:\GDRIVE\ya\tfinal\desarrollo\CircStat2012a');
rad2deg(circ_mean(deg2rad(h),[],2))
[s,s0] = circ_std(deg2rad(h),[],[],2);
s0*360/(2*pi)