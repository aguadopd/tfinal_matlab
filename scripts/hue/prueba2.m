h = [90, 90 , 91, 95, 98, 99, 120, 125, 345,345,345];
% h = [341, 355, 355, 10, 11, 11];

h1 = h;
media1 = geomean(h1);
desviacion1 = std(h1);

% Un nuevo vector c2, con el 0 en la media real (tal vez las ant�podas
% de la media correcta.
h2 = mod(h-media1, 360);
media2 = mean(h2);
desviacion2 = std(h2);

% La correcta es aquella opci�n cuya varianza es la m�nima.
if desviacion1 <= desviacion2
    desviacion = desviacion1;
    media = media1;
else
    desviacion = desviacion2;
    media = mod((media2+media1),360);
end


desviacion
media

figure();
rose(deg2rad(h1),40);
figure();
rose(deg2rad(h2),40);
