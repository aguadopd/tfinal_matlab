%% La configuraci�n usada est� en config.iat
% vid.FramesPerTrigger = 1;
% vid.ReturnedColorspace = 'rgb';
% triggerconfig(vid, 'manual');
% vid.TriggerRepeat = Inf;
% src.BacklightCompensation = 'off';
% src.Brightness = 12;
% src.FocusMode = 'manual';
% src.Contrast = 35;
% src.Brightness = 20;
% src.Gain = 0;
% src.Saturation = 86;
% vid.ROIPosition = [66 98 947 1102];
% src.Sharpness = 4;
% src.Hue = 0;

%% Guardado
% vvv es el arreglo exportado desde imaqtool
arreglo = vvv;
carpeta_destino = '../../../imagenes/set2';

% N�mero de inicio de los nombres de las im�genes guardadas
numero_inicio = 61;

j = 1; % aux
for i = 1 : 2 : size(arreglo,4)    
    imwrite(arreglo(:,:,:,i), [carpeta_destino, '/', sprintf('%02d',j+numero_inicio-1),'f','.png']);
    imwrite(arreglo(:,:,:,i+1), [carpeta_destino, '/', sprintf('%02d',j+numero_inicio-1),'r','.png']);
    j = j+1;
end