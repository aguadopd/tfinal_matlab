function [struct_parametros] = parametrosAStruct(principal_set)
%PARAMETROSASTRUCT [ALMENDRAS]
% Uso: [struct_parametros] = parametrosAStruct(principal_set)
% *struct_parametros*: Struct
% *principal_set*: Objeto de la clase PrincipalSet
%
% Esta funci�n crea una estructura 'struct_parametros' que contiene los
% nombres y valores de todos los par�metros variables que tiene cada etapa
% del algoritmo de 'principal_set'.
%
% Es usada por guardarParametros
%
% Ejemplo de elemento de estructura:
%     struct_parametros.PreprocesadorBlur.sigma

%% Validaci�n de entradas
% ToDo

%% Llenado
% Estructura:
%   etapa(Clase).parametro = valor
struct_parametros = struct();

n_algoritmo = length(principal_set.algoritmo);

% Para todas las etapas del algoritmo
for i = 1:n_algoritmo
    
    % Se indexan los par�metros seg�n la clase de la etapa a la que
    % corresponden. ATENCI�N: ESTO NO CONTEMPLA LA POSIBILIDAD DE QUE
    % EXISTAN VARIAS ETAPAS DE LA MISMA CLASE.
    clase = class(principal_set.algoritmo{i});
    
    parametros = fields(principal_set.algoritmo{i}.p);
    n_parametros = length(parametros);
    
    % Llenado de la estructura
    for j = 1:n_parametros
        nombre_parametro = parametros{j};
        struct_parametros.(clase).(nombre_parametro) = principal_set.algoritmo{i}.p.(nombre_parametro).valor;
    end
end

end