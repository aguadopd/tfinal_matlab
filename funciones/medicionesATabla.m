function [tabla] = medicionesATabla(principal_set, resultados)
%MEDICIONESATABLA [ALMENDRAS] Guarda valores medidos.
% Uso: [tabla] = medicionesATabla(principal_set, resultados)
% *tabla*: table de Matlab
% *principal_set*: Objeto de la clase PrincipalSet
% *resultados*: Objeto de la clase Resultados
%
% Usada por guardarMediciones. Genera una tabla con las mediciones que hizo
% cada etapa de 'principal_set' sobre los elementos de 'resultados'.

%% Validaci�n de entradas
% ToDo

%% Parsing
% Para cada etapa del algoritmo

% L�mites
n_resultados = length(resultados);
n_algoritmo = length(principal_set.algoritmo);

% Auxiliar. Formato: {nombre etapa, valores}
% Guarda las mediciones de cada etapa para todos los objetos en 'resultados'.
cell1 = cell(0);

% Para cada etapa del algoritmo de principal_set
for i = 1:n_algoritmo
    cell1{i, 1} = principal_set.algoritmo{i}.nombre_objeto;
    
    % Cell temporal donde se guardar�n las mediciones que hizo esta etapa
    % (si las hay) para cada Resultado en resultados
    temp1 = cell(n_resultados, 1);
    
    % Para cada Resultado en resultados
    for j = 1:n_resultados
        m = length(resultados{j}.internos);
        
        % Si hay internos
        if m > 0
            % Para cada interno
            for k = 1:m
                % Si el interno corresponde a la etapa
                if strcmp(resultados{j}.internos{k}.nombre_objeto, principal_set.algoritmo{i}.nombre_objeto)
                    % Si el interno tiene un campo valor
                    if any(strcmp(fields(resultados{j}.internos{k}), 'valor'))
                        % Se guarda para este Resultado
                        temp1{j} = resultados{j}.internos{k}.valor;
                    end
                    break
                end
            end
        end
    end
    
    % temp1 est� lleno. Se guarda en cell1
    cell1{i, 2} = temp1;
end

%% Tabla
% Columnas tabla: n, clase real, clase estimada, causa de rechazo, valores por etapa
tabla = table();

% Columnas
%  1. Vector n�meros �ndice
nn = cell(n_resultados,1);
for i = 1:n_resultados
    [~,nombrearchivo,~] = fileparts(principal_set.datos_originales{i,1});
    prefijo = sscanf(nombrearchivo,'%d');
    nn{i} = prefijo;
end
tabla.n = nn;

%  2. Clases reales
clases_reales = principal_set.datos(:, 5);
tabla.clase_real = clases_reales;

%  3. Clases estimadas
clases_estimadas = cell(n_resultados, 1);
% Carga de resultados y conversi�n a string
for i = 1:n_resultados
    clases_estimadas{i} = char(resultados{i}.clase);
end
tabla.clase_estimada = clases_estimadas;

%  4. Causa de rechazo
causas_rechazo = cell(n_resultados,1);
for i = 1:n_resultados
    causas_rechazo{i} = resultados{i}.causa_rechazo;
end
tabla.causa_rechazo = causas_rechazo;

%  5. Valores por etapa
%    matlab.lang.makeValidName fuerza un nombre v�lido para cada columna.
%    S�lo caracteres ASCII b�sicos, sin espacios.
for i = 1:size(cell1, 1)
    tabla.(matlab.lang.makeValidName(cell1{i, 1})) = cell1{i, 2};
end

end