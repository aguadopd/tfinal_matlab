function [mascara] = segmentarRetroiluminacion(imagen_retroiluminacion)
%SEGMENTARRETROILUMINACION [ALMENDRAS]
% Esta es una funci�n auxiliar utilizada para pregenerar las m�scaras
% binarias del set3. El proceso realizado es el mismo que el de
% SegmentadorRetroiluminaci�n, con 
%   sigma_blur = 2
%   radio_erosion = 1
%
% Se usa en los scripts 'recortar*'.
%
% Genera m�scaras binarias a partir de im�genes de objetos
% retroiluminados, donde el contraste entre el fondo y el objeto es
% grande.
%
% La imagen retroiluminada RGB se difumina con un filtro gaussiano, se
% umbraliza seg�n Otsu y luego se erosiona. Por �ltimo, se selecciona
% el objeto m�s grande y se rellenan sus agujeros, si los hay.


% Validaci�n de entradas
%- La imagen es a color, 3 canales: RGB
validateattributes(imagen_retroiluminacion, {'uint8'}, {'nonempty', 'ndims', 3, 'size', [NaN, NaN, 3]}, ...
    'segmentar', 'imagen', 1);

% Conversi�n a gris
g = rgb2gray(imagen_retroiluminacion);

% Blur gaussiano
filtrada = imgaussfilt(g, 2);

% Umbralizaci�n seg�n Otsu
umbral = graythresh(filtrada);
m = im2bw(filtrada, umbral);

% Inversi�n
m = ~m;

% Erosi�n
SE = strel('disk', 1);
m = imerode(m, SE);

% Segmentaci�n por componentes conectados. Dejamos el segmento m�s grande.
% Segmentaci�n de componentes conectados en la imagen binaria
CC = bwconncomp(m);
L = labelmatrix(CC);


% Solo queda el objeto m�s grande. Se supone que el resto son errores.
numPixels = cellfun(@numel, CC.PixelIdxList);
[~, idx] = max(numPixels);
mascara = (L == idx);


% Relleno
mascara = imfill(mascara, 'holes');

end