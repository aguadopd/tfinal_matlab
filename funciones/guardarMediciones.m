function guardarMediciones(archivo, principal_set, resultados)
%GUARDARMEDICIONES [ALMENDRAS] Guarda mediciones en un archivo
% Uso: guardarMediciones(archivo, PS, R)
% *archivo*: ruta y nombre de archivo tabla, con extensi�n
% *principal_set*: Objeto de la clase PrincipalSet
% *resultados*: Objeto de la clase Resultados
%
% Ej: guardarMediciones('../../med1.xls', ps, r);

% Creaci�n de la tabla
tabla = medicionesATabla(principal_set, resultados);

% Se borra, si existe. No queremos crear hojas nuevas, por ejemplo.
delete(archivo);

% Escritura
writetable(tabla, archivo);

end