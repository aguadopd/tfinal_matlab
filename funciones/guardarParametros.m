function guardarParametros(archivo, ps)
%GUARDARPARAMETROS [ALMENDRAS] Guardar par�metros a un archivo, en formato
%YAML.
% Uso: guardarParametros( archivo, ps )
% *archivo*: Nombre del archivo a guardar, con extensi�n .yml o .yaml
% inclu�da. Ej: '../../config.yml'
% *ps*: objeto de la clase PrincipalSet
%
% *Dependencias*: yamlmatlab https://github.com/ewiger/yamlmatlab
%
% Permite crear un archivo de configuraci�n con los par�metros de cada una
% de las etapas de un objeto ps de la clase PrincipalSet. El archivo de
% configuraci�n est� en formato YAML. Un ejemplo:
%
%     PreprocesadorBlur:
%       sigma: 2.0
%     SegmentadorBorde1:
%       cierre_radio: 1.0
%       cierre_largo: 1.0

% Validaci�n de entradas
% ToDo

% Creaci�n de struct, necesaria para yamlmatlab
s = parametrosAStruct(ps);

% Se usa yaml. porque la carpeta tiene un + inicial (+yaml) y eso la
% declara como un paquete.
yaml.WriteYaml(archivo, s);

end