function [ R ] = evaluar( i_rgb )
%EVALUAR [ R ] = evaluar( i_rgb )
%   i_rgb es una imagen rgb, que será sometida a todo el proceso de
%   clasificación. Esto implica preprocesamiento, segmentación, y
%   clasificación por una cascada de clasificadores. R es un objeto de la
%   clase Resultados que se va llenando a lo largo de la función.

%% Creación de objetos

% Segmentador
S = Segmentador();

% Objeto Resultados que se va a ir llenando
R = Resultados( i_rgb );



%% Procesamiento inicial

% Segmentación
R = S.segmentar(R);



%% Clasificación

% Si no hay objeto para clasificar, sale
if R.hay_objeto == false
    return
end
    


end