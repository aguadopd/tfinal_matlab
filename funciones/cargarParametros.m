function cargarParametros(archivo, ps)
%CARGARPARAMETROS [ALMENDRAS] Carga de par�metros desde archivo
% Uso: cargarParametros( archivo, ps )
% *archivo*: Ruta del archivo a cargar: '../config.yaml'
% *ps*: Objeto de la clase PricipalSet
%
% *Dependencias*: yamlmatlab https://github.com/ewiger/yamlmatlab
%
% Permite cargarle par�metros a un objeto de la clase PrincipalSet, desde
% un archivo YAML con los valores de cada etapa. Por ejemplo:
%
%     PreprocesadorBlur:
%       sigma: 2.0
%     SegmentadorBorde1:
%       cierre_radio: 1.0
%       cierre_largo: 1.0
%
% Si existe en ps una etapa con alguna de las clases listadas en el
% archivo, se cargar�n los valores para sus par�metros listados.
%
% ToDo: No est� contemplado que haya m�s de una etapa con la misma clase.

%%

% Validaci�n de entradas
% ToDo

% Carga de archivo con formato YAML.
% s es struct
s = yaml.ReadYaml(archivo);

% Llenado de ps
campos = fields(s);
n = length(campos);

m = length(ps.algoritmo);

for i = 1:n
    for j = 1:m
        if isa(ps.algoritmo{j}, campos{i})
            parametros = fields(s.(campos{i}));
            for k = 1:length(parametros)
                ps.algoritmo{j}.p.(parametros{k}).valor = s.(campos{i}).(parametros{k});
            end
        end
    end
end

end