function mostrarSegmentacion(resultados)
%MOSTRARSEGMENTACION [ALMENDRAS] Muestra resultados de la segmentaci�n
% Uso: mostrarSegmentacion( r )
% *r*: Objeto de la clase Resultados
%
% Muestra en pantalla una fusi�n m�scara-original para frente y dorso de
% cada uno de los objetos en r. Se avanza con cualquier tecla. Salida
% prematura con Ctrl+C

%% Validaci�n de entradas
% ToDo

%% 
n = length(resultados);
figure;

for i = 1:n
    % Informe de progreso
    fprintf('Imagen %d\n', i);
    
    % Fusi�n en falso color
    frente = imfuse(resultados{i}.imagen_original_frente, resultados{i}.mascara_frente);
    dorso = imfuse(resultados{i}.imagen_original_dorso, resultados{i}.mascara_dorso);
    
    % Lado a lado
    ambas = imfuse(frente, dorso, 'montage');
    
    % Texto inserto. Tal vez, innecesario.
    ambas = insertText(ambas, [50, 50], sprintf('Imagen %d\n', i), 'FontSize', 30);
    %     text(50,50,sprintf('Imagen %d\n',i)); % Esto es anotaci�n.
    
    % Show
    imshow(ambas);
    
    pause;
end