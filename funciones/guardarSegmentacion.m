function guardarSegmentacion(resultados, carpeta)
%GUARDARSEGMENTACION [ALMENDRAS] Guarda fusi�n original-mascara
% Uso: guardarSegmentacion(resultados, carpeta)
% *resultados*: Objeto de la clase Resultados
% *carpeta*: Carpeta en donde se guardar�n las im�genes. Si no existe, se
% crea.
%
% Ej: guardarSegmentacion(r, '../temp');

%% Validaci�n de entradas
% ToDo

%%
% Se crea la carpeta, si no existe
mkdir(carpeta)

n = length(resultados);

% Las im�genes se guardan en .jpg
for i = 1:n
    % Informe de progreso
    fprintf('Imagen %d\n', i);
    
    % Fusi�n en falso color
    frente = imfuse(resultados{i}.imagen_original_frente, resultados{i}.mascara_frente);
    dorso = imfuse(resultados{i}.imagen_original_dorso, resultados{i}.mascara_dorso);
    
    % Lado a lado
    ambas = imfuse(frente, dorso, 'montage');
    
    % N�mero inserto. Tal vez innecesario.
    ambas = insertText(ambas, [50, 50], sprintf('Imagen %d\n', i), 'FontSize', 30);
    
    % Escritura
    imwrite(ambas, fullfile(carpeta, [sprintf('%d', i), '.jpg']));
end
disp('Imagenes guardadas');
end