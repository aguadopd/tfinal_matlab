function crearSubconjuntos(dir_archivo_set, proporcion, ...
    dir_archivo_entrenamiento, dir_archivo_prueba)
%CREARSUBCONJUNTOS [ALMENDRAS] Divide en conjuntos de entrenamiento y de prueba.
% Uso: crearSubconjuntos( dir_archivo_set, proporcion, ...
% dir_archivo_entrenamiento, dir_archivo_prueba)
%
% *INCLUIR EL FORMATO EN LOS NOMBRES DE ARCHIVO*
%
% Ej: crearSubconjuntos('..\..\..\imagenes\set2\set2_1.csv', ...
% 0.1,'..\..\..\imagenes\set2\set2_1_e.csv','..\..\..\imagenes\set2\set2_1_p.csv')
%
% Carga lista de archivos y la divide en conjuntos de
% entrenamiento y de prueba. Proporci�n es la fracci�n del total que se
% usar� para pruebas. 0 < proporcion < 1%

%% Validaci�n de entradas
% ToDo

%% Carga de lista a tabla
formato = '%s%s%s%s%s';
delimitador = ';';
tabla = readtable(dir_archivo_set, 'Delimiter', delimitador, ...
    'Format', formato);

% Subtablas
fao = table2cell(tabla(:, 'f_archivo_original'));
fam = table2cell(tabla(:, 'f_archivo_mascara'));
dao = table2cell(tabla(:, 'd_archivo_original'));
dam = table2cell(tabla(:, 'd_archivo_mascara'));
clases = table2cell(tabla(:, 'clase'));

%% Divisi�n
c = cvpartition(clases, 'holdout', proporcion);
indices_prueba = c.test;
indices_entrenamiento = c.training;

% Tablas
t_prueba = table(fao(indices_prueba), fam(indices_prueba), dao(indices_prueba), ...
    dam(indices_prueba), clases(indices_prueba), 'VariableNames', ...
    {'f_archivo_original', 'f_archivo_mascara', ...
    'd_archivo_original', 'd_archivo_mascara', 'clase'});

t_entrenamiento = table(fao(indices_entrenamiento), fam(indices_entrenamiento), ...
    dao(indices_entrenamiento), dam(indices_entrenamiento), ...
    clases(indices_entrenamiento), 'VariableNames', ...
    {'f_archivo_original', 'f_archivo_mascara', ...
    'd_archivo_original', 'd_archivo_mascara', 'clase'});

% Escritura. Los nombres de archivo deben incluir el formato.
writetable(t_prueba, dir_archivo_prueba, 'WriteVariableNames',...
    true, 'Delimiter', delimitador);
writetable(t_entrenamiento, dir_archivo_entrenamiento, 'WriteVariableNames',...
    true, 'Delimiter', delimitador);

end