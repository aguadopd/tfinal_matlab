carpeta_base = 'D:\GDRIVE\ya\tfinal\desarrollo\tfinal_matlab';
carpeta_imagenes = 'D:/tfinal/imagenes/';
carpeta_experimentos = fullfile(carpeta_base,'experimentos');

addpath(genpath(fullfile(carpeta_base,'clases')));
addpath(genpath(fullfile(carpeta_base,'funciones')));
addpath(genpath(fullfile(carpeta_base,'GUI')));
addpath(genpath(fullfile(carpeta_base,'otros')));
addpath(genpath(carpeta_imagenes));