%% Confusion Matrix and its Derivations
%% Syntax
% |C = multic(A,B)|
%
% |C = multic(A,B,MODE)|
%
% |C = multic(A,B,MODE,BETA)|
% 
% |[C,T] = multic(A,...)|
%
% |[C,T,D] = multic(A,...)|
% 
% |[C,T,D,M] = multic(A,...)|
% 
% |[C,T,D,M,N] = multic(A,...)|
%  
% |[C,T,D,M,N,ORDER] = multic(A,...)|
%
%% Introduction
% Using results of multiclass classification the function builds a confusion
% matrix and tables of confusion and calculates derivations from
% a confusion matrix for each class and for all population. Derivations are
% metrics of classifier's performance. Each derivation serves different
% purposes. Consideration of derivations allows a comprehensive study of
% classifier's performance.
%% Description
%
% |C = multic(A,B)| returns the _n_ X _n_ confusion matrix _C_ determined  
% by the known and predicted groups in _A_ and _B_ , respectively. _A_ and 
% _B_ are input vectors with the same number of observations. _n_ is 
% the total number of distinct elements in _A_ and _B_. Input vectors must
% be of the same type and can be numeric arrays or cell arrays of strings.
% By default missing classification (observation) is not counted. Missing
% classification must be denoted as NaN for numeric type and be empty
% string for cell array of strings. Calculations are case insensitive. All 
% strings will be transformed to lower case.
% 
% |C = multic(A,B,MODE)|, where |MODE = 1| calculates missing classification 
% as "false negative" for class corresponding missing classification and
% "true negative" for other classes. |MODE = 0| is default.
%
% |C = multic(A,B,1)| returns _n_ X _(n+2)_ augmented confusion matrix. 
% Column _n_ +1 contains NaN in a row corresponding a class with missing 
% classification and column _n_ +2 contains a number of missing 
% classifications for each class.
%
% |C = multic(A,B,MODE,BETA)|, |BETA| is a value for _F_ -score. Default 
% value is |BETA = 1.|
%
% |[C,T] = multic(A,...)| returns _C_ and _T_ , where _T_ is 2X2 confusion 
% table:
%
% <html>
% <table border=1><tr><td><i>TP</td><td><i>FN</td></tr>
% <tr><td><i>FP</td><td><i>TN</td></tr></table>
% </html>
%
% _TP_ is true positive (aka hit), _FP_ is false
% positive (aka false alarm, Type I error), _FN_ is false negative (aka 
% miss, Type II error) and _TN_ is true negative (aka correct rejection).
%
% |[C,T,D] = multic(A,...)| returns as above and _D_, where _D_
% is a 18-vector of derivations from a confusion matrix. 
%
% They are:
%
% # Accuracy,
% # Precision (positive predictive value),
% # False discovery rate,
% # False omission rate,
% # Negative predictive value,
% # Prevalence,
% # Recall (hit rate, sensitivity, true positive rate),
% # False positive rate (fall-out),
% # Positive likelihood ratio,
% # False negative rate (miss rate),
% # True negative rate (specificity),
% # Negative likelihood ratio,
% # Diagnostic odds ratio,
% # Informedness,
% # Markedness,
% # F-score,
% # G-measure,
% # Matthews correlation coefficient.
%
% |[C,T,D,M] = multic(A,...)| returns as above and _M_ , where _M_ is a 
% 2 _n_ X2 matrix, which contains confusion tables for each class (one 
% versus all). Rows 2 _i_ -1 and 2 _i_ correspond to class with label 
% number _i_ , _i_ = 1, 2, ..., _n_ .
%
% |[C,T,D,M,N] = multic(A,...)| returns as above and _N_ , where _N_ is
% a 18 X _n_ matrix of derivations from confusion tables, which are 
% contained in M. Column _i_ corresponds to class with label number _i, 
% i = 1, 2, ..., n_ .
%
% |[C,T,D,M,N,ORDER] = multic(A,...)| also returns the order of the rows
% and columns of _C_ (class labels) in a variable ORDER the same type 
% as input vectors.
%
%% References
% * https://en.wikipedia.org/wiki/Confusion_matrix
% * http://araw.mede.uic.edu/cgi-bin/testcalc.pl
% * https://www.medcalc.org/calc/diagnostic_test.php
% * http://www.biochemia-medica.com/content/odds-ratio-calculation-usage-and-interpretation
%% Examples
% |*_Example 1:_*| calculation of the confusion matrix for data with one
% misclassification and one missing classification.
A = [1 1 1 2 2 2 2 2 3  3 ];  % Known groups
B = [1 1 1 2 2 2 3 4 4 NaN];  % Predicted groups
disp('Missing classification is not counted:')
C = multic(A,B)   % MODE=0
disp('Missing classification is counted:')
C = multic(A,B,1) % MODE=1
%%
% |*_Example 2:_*|
% calculation of the confusion matrix, confusion table, derivations of it
% and confusion tables and derivations for each class for data with three
% misclassifications and two missing classifications. 
% Missing classifications are counted. 
A=[1 1 3 1 1 2 2 2 2 2  2   2  3 1 3 3];  % Known groups
B=[1 1 1 1 1 2 2 2 2 1 NaN NaN 2 1 3 3];  % Predicted groups
[C,T,D,M,N,order] = multic(A,B,1)
%% 
% |*_Example 3:_*|
% calculation of the confusion matrix for data with two misclassifications 
% and one missing classification. 
A = {'Cats','Cats','Rats','Rats','Rats','Rabbits','Rabbits'};  % Known groups
B = {'Cats','Cats','Rats','Rats','Rabbits','Rats',''};  % Predicted groups
[C,~,~,~,~,order] = multic(A,B,1)
