function varargout = gui(varargin)
% GUI MATLAB code for gui.fig [ALMENDRAS]
%      Este es el archivo de configuraci�n de la interfaz gr�fica creada
%      para el sistema de clasificaci�n de almendras. 
%
%      En la funci�n inmediatamente inferior (gui_OpeningFcn) se pueden
%      editar los par�metros que definen cu�l es el conjunto de im�genes
%      que se va a evaluar y cu�les ser�n las etapas del algoritmo.
%
%      ----------------------------------------------------------------
%      GUI, by itself, creates a new GUI or raises the existing
%      singleton*.
%
%      H = GUI returns the handle to a new GUI or the handle to
%      the existing singleton*.
%
%      GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUI.M with the given input arguments.
%
%      GUI('Property','Value',...) creates a new GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before gui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to gui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help gui

% Last Modified by GUIDE v2.5 03-May-2017 22:24:56

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @gui_OpeningFcn, ...
                   'gui_OutputFcn',  @gui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT





% --- Executes just before gui is made visible.
function gui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to gui (see VARARGIN)

% Choose default command line output for gui
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes gui wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% Add paths
try
    carpeta_imagenes = evalin('base','carpeta_imagenes');
catch
    error('Debe haber una variable "carpeta_imagenes" definida en el workspace.');
end

% -------------------------------------------------------------------------------
% -------------------------------------------------------------------------------
% VARIABLES SELECCIONABLES


% CONJUNTOS DE IM�GENES A USAR - descomentar seg�n corresponda.
% Set 1. Usar con segmentador por matiz.
% handles.PS = PrincipalSet('set1', fullfile(carpeta_imagenes,'\set1\set1.csv'));

% Set 2. Usar con segmentador por borde.
% handles.PS = PrincipalSet('set2', fullfile(carpeta_imagenes,'\set2\set2.csv'));

% Set 3. Usar con segmentador seg�n imagen retroiluminada.
%   Completo: se sugiere no usar. Tarda mucho y ocupa much�sima memoria.
% handles.PS = PrincipalSet('set3', fullfile(carpeta_imagenes,'/set3/set3.csv'));
%   Parcial 20%.
% handles.PS = PrincipalSet('set3', fullfile(carpeta_imagenes,'/set3/set3_b_8020_eval.csv'));

% Set 3 recortado y con m�scaras binarias precalculadas. Usar segmentador
% bobo.
%   Completo
% handles.PS = PrincipalSet('set3', fullfile(carpeta_imagenes,'/set3/sindist_rec_bin/set3.csv'));
%   Parcial 20%.
handles.PS = PrincipalSet('set3', fullfile(carpeta_imagenes,'/set3/sindist_rec_bin/set3_b_8020_eval.csv'));


% CONFIGURACI�N DE ALGORITMO
% Lo cargamos con preprocesador, segmentador y clasificadores
handles.PS.agregar( PreprocesadorBlur('Blur gaussiano') );
% handles.PS.agregar( SegmentadorHue1('Segmentador por matiz') );
% handles.PS.agregar( SegmentadorBorde1('Segmentador por borde') );
% handles.PS.agregar( SegmentadorRetroiluminacion('Segmentador retroiluminaci�n', handles.PS.datos) );
handles.PS.agregar( SegmentadorBobo('Segmentador bobo', handles.PS.datos) );
handles.PS.agregar( ClasificadorAltura('altura') );
handles.PS.agregar( ClasificadorExtensionBB('extent') );
handles.PS.agregar( ClasificadorElipseRA('relaci�n de aspecto de elipse') );
% handles.PS.agregar( ClasificadorCajaRA('relaci�n de aspecto de caja') );
% handles.PS.agregar( ClasificadorRaspaduras('�rea de raspaduras') );
handles.PS.agregar( ClasificadorRaspaduras2('�rea de raspaduras') );
handles.PS.agregar( ClasificadorSolidez('solidez') );
handles.PS.agregar( ClasificadorHueMedia('media de color ') );
handles.PS.agregar( ClasificadorCIELabMedia('medias a y b CIELab') );
handles.PS.agregar( ClasificadorHueDesviacion('desviaci�n est�ndar de color') );
% handles.PS.agregar( ClasificadorArrugas('arrugas') );

% -------------------------------------------------------------------------------
% -------------------------------------------------------------------------------

% PROCESAMIENTO
handles.r = handles.PS.procesarTodo(true);
handles = configurar_tras_carga(hObject,handles);
handles = configurar_parametros(handles);


cargar_etapas(handles);
handles = procesar_y_dibujar(handles);
guidata(hObject,handles);

%%




% --- Outputs from this function are returned to the command line.
function varargout = gui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in etapas.
function etapas_Callback(hObject, eventdata, handles)
% hObject    handle to etapas (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns etapas contents as cell array
%        contents{get(hObject,'Value')} returns selected item from etapas
configurar_parametros(handles);
guidata(hObject,handles);






%
function handles = configurar_parametros(handles)
parametros = handles.PS.algoritmo{handles.etapas.Value}.p; 
campos = fieldnames(parametros); 
n = length(campos);

% En este momento 10 es el m�ximo soportado
maxparam = 10;
if n > maxparam
    n = maxparam;
end

% Encendemos par�metros a usar
for i = 1:n
    p = ['param',sprintf('%d',i)];
    t = [p,'text'];
    handles.(p).Visible = 'on';
    handles.(p).String = parametros.(campos{i}).valor;
    handles.(p).Value = i;
   
    handles.(t).Visible = 'on';
    handles.(t).String = campos{i};
end
% Apagamos par�metros no usados.
if n < maxparam
    for i = (n+1):maxparam
        p = ['param',sprintf('%d',i)];
        t = [p,'text'];
        handles.(p).Visible = 'off';
        handles.(t).Visible = 'off';
    end
end





% --- Executes during object creation, after setting all properties.
function etapas_CreateFcn(hObject, eventdata, handles)
% hObject    handle to etapas (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% 
function handles = cargar_etapas(handles)
    n = length(handles.PS.algoritmo);
    nombres = cell(n,1);
    
    for i = 1:n
        nombres{i} = handles.PS.algoritmo{i}.nombre_objeto;
    end
    
    set(handles.etapas,'string',nombres); 
    set(handles.etapas,'max',1);



% --- Executes on slider movement.
function slider1_Callback(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
val=round(hObject.Value);
hObject.Value=val;
handles.n = val;
set(handles.editn,'String',handles.n);
handles = procesar_y_dibujar(handles);
guidata(hObject,handles);



% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end





function editn_Callback(hObject, eventdata, handles)
% hObject    handle to editn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editn as text
%        str2double(get(hObject,'String')) returns contents of editn as a double
s = str2double(get(hObject,'String'));
handles.n = round(s);
handles.slider1.Value = handles.n;
handles = procesar_y_dibujar(handles);
guidata(hObject,handles);



% --- Executes during object creation, after setting all properties.
function editn_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
hObject.String = '1';




function handles = procesar_y_dibujar(handles)
a_mostrar = handles.popupmenu1.String{handles.popupmenu1.Value};

% Procesar Resultado
handles.r{handles.n} = handles.PS.procesarResultado(handles.r{handles.n},true);

img1 = [];
img2 = [];

% Mostrar
switch a_mostrar
    case 'Original'
        img1 = handles.r{handles.n}.imagen_original_frente;
        img2 = handles.r{handles.n}.imagen_original_dorso;
    case 'M�scara'
        img1 = handles.r{handles.n}.mascara_frente;
        img2 = handles.r{handles.n}.mascara_dorso;
    case 'Fusi�n'
        img1 = imfuse(handles.r{handles.n}.imagen_original_frente,...
            handles.r{handles.n}.mascara_frente,'falsecolor');
        img2 = imfuse(handles.r{handles.n}.imagen_original_dorso,...
            handles.r{handles.n}.mascara_dorso,'falsecolor');
    case 'Fusi�n preprocesada'
        img1 = imfuse(handles.r{handles.n}.imagen_frente,...
            handles.r{handles.n}.mascara_frente,'falsecolor');
        img2 = imfuse(handles.r{handles.n}.imagen_dorso,...
            handles.r{handles.n}.mascara_dorso,'falsecolor');
    case 'Original y m�scara'
        img1 = imfuse(handles.r{handles.n}.imagen_original_frente,...
            handles.r{handles.n}.mascara_frente,'montage');
        img2 = imfuse(handles.r{handles.n}.imagen_original_dorso,...
            handles.r{handles.n}.mascara_dorso,'montage');
    case 'Preprocesada y m�scara'
        img1 = imfuse(handles.r{handles.n}.imagen_frente,...
            handles.r{handles.n}.mascara_frente,'montage');
        img2 = imfuse(handles.r{handles.n}.imagen_dorso,...
            handles.r{handles.n}.mascara_dorso,'montage');
    case 'Etapa'
        campos = fields(handles.r{handles.n}.internos{handles.etapas.Value});
        if any(strcmp(campos,'imagen_frente'))
            img1 = handles.r{handles.n}.internos{handles.etapas.Value}.imagen_frente;
        else 
            img1 = [];
        end
        if any(strcmp(campos,'imagen_frente'))
            img2 = handles.r{handles.n}.internos{handles.etapas.Value}.imagen_dorso;
        else 
            img2 = [];
        end
        
    otherwise
        img1 = handles.r{handles.n}.imagen_original_frente;
        img2 = handles.r{handles.n}.imagen_original_dorso;
end
    
axes(handles.axes1);
imshow(img1);
axes(handles.axes2);
imshow(img2);

% s = handles.cajatexto.String;

clase = char(handles.r{handles.n}.clase);

s = handles.r{handles.n}.log;

handles.cajatexto.String = s;

handles.textoclaseestimada.String = clase;
handles.textoclasereal.String = handles.PS.datos{handles.n,5};

if ~strcmp(handles.textoclaseestimada.String,handles.textoclasereal.String)
    handles.textoclaseestimada.ForegroundColor = [1,0,0];
else
    handles.textoclaseestimada.ForegroundColor = [0,0,0];
end
% guidata(hObject,handles);



function handles = configurar_tras_carga(hObject,handles)
handles.slider1.Max = length(handles.r);
handles.slider1.Min = 1;
handles.slider1.Value = handles.slider1.Min;
p = 1/handles.slider1.Max;
handles.slider1.SliderStep = [p,p];
handles.n = handles.slider1.Value;
guidata(hObject,handles);
configurar_parametros(handles);


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1
procesar_y_dibujar(handles);



% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
hObject.String = {  'Original',...
                    'M�scara',...
                    'Fusi�n',...
                    'Fusi�n preprocesada',...
                    'Original y m�scara',...
                    'Preprocesada y m�scara',...
                    'Etapa'};



function param1_Callback(hObject, eventdata, handles)
% hObject    handle to param1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of param1 as text
%        str2double(get(hObject,'String')) returns contents of param1 as a double

% TODO: Validar entradas
callback_parametros(hObject,handles);
% guidata(hObject,handles);


% --- Executes during object creation, after setting all properties.
function param1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to param1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function param2_Callback(hObject, eventdata, handles)
% hObject    handle to param2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of param2 as text
%        str2double(get(hObject,'String')) returns contents of param2 as a double
callback_parametros(hObject,handles);


% --- Executes during object creation, after setting all properties.
function param2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to param2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function param3_Callback(hObject, eventdata, handles)
% hObject    handle to param3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of param3 as text
%        str2double(get(hObject,'String')) returns contents of param3 as a double
callback_parametros(hObject,handles);


% --- Executes during object creation, after setting all properties.
function param3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to param3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function param4_Callback(hObject, eventdata, handles)
% hObject    handle to param4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of param4 as text
%        str2double(get(hObject,'String')) returns contents of param4 as a double
callback_parametros(hObject,handles);


% --- Executes during object creation, after setting all properties.
function param4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to param4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function param5_Callback(hObject, eventdata, handles)
% hObject    handle to param5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of param5 as text
%        str2double(get(hObject,'String')) returns contents of param5 as a double
callback_parametros(hObject,handles);


% --- Executes during object creation, after setting all properties.
function param5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to param5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function param6_Callback(hObject, eventdata, handles)
% hObject    handle to param6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of param6 as text
%        str2double(get(hObject,'String')) returns contents of param6 as a double
callback_parametros(hObject,handles);


% --- Executes during object creation, after setting all properties.
function param6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to param6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function param7_Callback(hObject, eventdata, handles)
% hObject    handle to param7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of param7 as text
%        str2double(get(hObject,'String')) returns contents of param7 as a double
callback_parametros(hObject,handles);


% --- Executes during object creation, after setting all properties.
function param7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to param7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function param8_Callback(hObject, eventdata, handles)
% hObject    handle to param8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of param8 as text
%        str2double(get(hObject,'String')) returns contents of param8 as a double
callback_parametros(hObject,handles);


% --- Executes during object creation, after setting all properties.
function param8_CreateFcn(hObject, eventdata, handles)
% hObject    handle to param8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function param9_Callback(hObject, eventdata, handles)
% hObject    handle to param9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of param9 as text
%        str2double(get(hObject,'String')) returns contents of param9 as a double
callback_parametros(hObject,handles);


% --- Executes during object creation, after setting all properties.
function param9_CreateFcn(hObject, eventdata, handles)
% hObject    handle to param9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function param10_Callback(hObject, eventdata, handles)
% hObject    handle to param10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of param10 as text
%        str2double(get(hObject,'String')) returns contents of param10 as a double
callback_parametros(hObject,handles);


% --- Executes during object creation, after setting all properties.
function param10_CreateFcn(hObject, eventdata, handles)
% hObject    handle to param10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end





function handles = callback_parametros(hObject,handles)
% TODO: Validaci�n de entradas (hObject.String). Chequeo de tipos.
handles.PS.algoritmo{handles.etapas.Value}.p.(handles.(['param',sprintf('%d',hObject.Value),'text']).String).valor...
    = str2double(hObject.String);
handles = procesar_y_dibujar(handles);
guidata(hObject,handles);
