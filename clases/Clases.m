classdef Clases < int32
%CLASES [ALMENDRAS] Enumeraci�n de clases posibles
%   Esta clase enumera las diferentes clases o etiquetas de los objetos
%   que ver� el sistema.
%
%   Es subclase de int32 para posibilitar comparaciones relacionales
%   del tipo <, >, =>, ...
% 
%   Se puede castear a char con char(objeto). 
%   Ej: >> c = Clases.Primera;
%       >> char(c)
%            ans = 'Primera'
    
    %% Clases
    enumeration
        % Entre par�ntesis, el valor num�rico asignado.
        
        % Nada. Tal vez no se use.
        Nada(0)
        % Objeto extra�o.
        NoAlmendra(1)
        % Almendra mala.
        Mala(2)
        % Almendra de segunda clase. No es tan mala, pero no es perfecta.
        Segunda(3)
        % Almendra de primera clase. Una buena almendra.
        Primera(4)
    end
    
    %% Propiedades
    properties
    end
    
    %% M�todos
    methods
    end
    
end