classdef SegmentadorBobo < Segmentador
    %SEGMENTADORBOBO Carga im�genes binarias como m�scaras
    % Este segmentador no procesa nada. Carga im�genes RGB con p�xeles en
    % blanco o negro, como m�scaras.
    %
    % Carga estas im�genes desde las direcciones que se encuentran en el
    % arreglo (cell) de datos de un objeto PrincipalSet. Este arreglo debe
    % pasarse como entrada.
    
    
    %% Propiedades p�blicas
    % Todos los par�metros que se puedan variar durante la ejecuci�n del
    % programa, probablemente a trav�s de la GUI.
    properties(SetAccess = public)
        % Arreglo tipo cell que contiene informaci�n sobre las im�genes que
        % se est�n procesando. Mismo formato que la propiedad 'datos' de la
        % clase PrincipalSet.
        cellarchivos
    end
    
    
    %% M�todos p�blicos
    methods
        
        
        function obj = SegmentadorBobo(nombre, cellarchivos, varargin)
            % Constructor
            % Ej: PS.agregar( SegmentadorBobo('bobo', PS.datos) );
            
            % Validaci�n de entradas
            % ToDo
            
            % Constructor de superclase
            obj@Segmentador(nombre);
            
            % Arreglo tipo cell que contiene informaci�n sobre las im�genes que
            % se est�n procesando. Mismo formato que la propiedad 'datos' de la
            % clase PrincipalSet.
            obj.cellarchivos = cellarchivos;
            
        end
        
        
    end
    
    
    %% M�todos protegidos
    methods(Access = protected)
        
        
        function [mascara_frente, mascara_dorso, hay_objeto, internos] = ...
                segmentarResultados(obj, Resultados)
            %SEGMENTARRESULTADOS Segmenta un objeto de clase resultados
            % Uso: [mascara_frente, mascara_dorso, hay_objeto, internos] = ...
            %                          ||||segmentarResultados(obj, resultados)
            % *mascara_frente*: imagen de tipo Logical. En blanco (true), los
            % p�xeles que corresponden al objeto.
            % *mascara_dorso*: imagen de tipo Logical. En blanco (true), los
            % p�xeles que corresponden al objeto.
            % *hay_objeto*: Booleano que indica si se encontr� algo. No est� en
            % uso.
            % *internos*: Estructura de informaci�n que emite la etapa.
            %   Por ejemplo, valores que midi�, o im�genes �tiles
            %   para depuraci�n.
            % *resultados*: Objeto de la clase Resultados.
            
            % Lectura y conversi�n a logical
            mascara_frente = logical(imread(obj.cellarchivos{Resultados.id_numero, 2}));
            mascara_dorso = logical(imread(obj.cellarchivos{Resultados.id_numero, 4}));
            
            % No se verifica si hay objeto.
            hay_objeto = true;
            
            % Estructura de resultados internos. Vac�a.
            internos = struct();
            
        end %
        
        
    end % methods protected
    
    
end % classdef