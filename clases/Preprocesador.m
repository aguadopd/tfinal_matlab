classdef Preprocesador
    %PREPROCESADOR [ALMENDRAS] Superclase para los preprocesadores
    % Los preprocesadores realizan modificaciones a las im�genes pero no
    % las clasifican. No necesariamente se ejecutan antes de la
    % segmentaci�n y clasificaci�n; pueden estar en cualquier punto del
    % algoritmo (y por tanto, deber�an llamarse 'procesadores').
    %
    % Cada objeto de clase Resultados tiene las siguientes propiedades
    % �tiles:
    %   * imagen_original_frente
    %   * imagen_original_dorso
    %   * imagen_frente
    %   * imagen_dorso
    %
    % Las originales son de solo lectura, pero pueden ser �tiles para
    % alguna etapa de preprocesamiento. Las otras son las que modifica cada
    % preprocesador y **SON LAS QUE LEEN LOS CLASIFICADORES**.
    
    
    %% Propiedades p�blicas
    % Todos los par�metros que se puedan variar durante la ejecuci�n del
    % programa, probablemente a trav�s de la GUI.
    properties(SetAccess = public)
        % Par�metros de cada preprocesador. 'p' para brevedad. Es una
        % estructura de objetos de la clase Parametro.
        p = struct();
    end
    
    
    %% Propiedades privadas
    properties(SetAccess = protected)
        % Nombre del objeto creado, para m�s f�cil identificaci�n.
        nombre_objeto = '';
    end
    
    
    %% M�todos p�blicos
    methods
        
        
        function obj = Preprocesador(nombre, varargin)
            % Constructor
            
            
            % Validaci�n de entradas
            % ToDo
            
            % Identificaci�n
            obj.nombre_objeto = nombre;
        end
        
        
        function resultados = procesar(obj, resultados)
            %PROCESAR Rellena objeto Resultados
            % Uso: resultados = procesar(obj, resultados)
            % *resultados* = Objeto de la clase Resultados
            %
            % Modifica las propiedades p�blicas 'imagen_frente' e
            % 'imagen_dorso' del objeto 'resultados'.
            
            % Procesado
            [img_frente, img_dorso, internos] = procesarResultados(obj, resultados);
            resultados.imagen_frente = img_frente;
            resultados.imagen_dorso = img_dorso;
            
            % Se guarda la estructura de metainformaci�n de esta etapa.
            internos.nombre_objeto = obj.nombre_objeto;
            resultados.internos{end+1} = internos;
            
        end % procesar
        
        
    end
    
    
    %% M�todos abstractos
    methods(Abstract = true, Access = protected)
        
        
        %PROCESARRESULTADOS Procesa un objeto de clase Resultados
        % Uso: [img_frente, img_dorso, internos] = procesarResultados(obj, resultados)
        % *img_frente*: imagen procesada.
        % *img_dorso*: imagen procesada.
        % *internos*: Estructura de informaci�n que emite la etapa.
        %   Por ejemplo, valores que midi�, o im�genes �tiles
        %   para depuraci�n.
        % *resultados*: Objeto de la clase Resultados.
        [img_frente, img_dorso, internos] = procesarResultados(obj, resultados)
        
        
    end
    
    
end % classdef