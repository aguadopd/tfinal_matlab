classdef ClasificadorHueMedia < Clasificador
    %CLASIFICADORHUEMEDIA [ALMENDRAS] Media del color seg�n H
    %
    % Descriptor: Media del matiz/tono del objeto. En caso de
    % ser almendras, puede forzarse el an�lisis a las zonas con tegumento
    % �nicamente. Esto �ltimo si la m�scara usada es la m�scara sin zonas
    % peladas.
    %
    % Este clasificador busca detectar almendras de color malo u objetos
    % que no sean almendras.
    %
    % La m�trica final es la media de las medias calculadas para el frente
    % y el dorso del objeto. Si esta se excede de ciertos valores l�mites,
    % se considera que es una almendra Mala,
    %
    %
    % PARAMETROS
    %   *umbral_min*: Valor m�nimo de media.
    %       0 <= umbral_min < umbral_max
    %   *umbral_max*: Valor m�ximo de media.
    %       umbral_min < umbral_max <= 360
    %   *radio_erosion*: Radio en p�xeles de un disco con el que se
    %   erosiona la m�scara antes de calcular, ya que no hay buena
    %   informaci�n de color en los bordes.
    %       0 <= radio_erosion <= 50
    
    
    %% Propiedades p�blicas
    % Todos los par�metros que se puedan variar durante la ejecuci�n del
    % programa, probablemente a trav�s de la GUI.
    properties(SetAccess = public)
    end
    
    
    %% Propiedades privadas
    properties(SetAccess = private)
    end
    
    
    %% M�todos p�blicos
    methods(Access = public)
        
        
        function obj = ClasificadorHueMedia(nombre, varargin)
            % Constructor
            
            % Validaci�n de entradas
            % ToDo
            
            % Constructor de superclase
            obj@Clasificador(nombre);
            
            % Par�metros
            % Par�metros variables de esta etapa. Son miembros de la
            % estructura p. Su formato:
            % Parametro(por_defecto, min, max, valor);
            
            %   *umbral_min*: Valor m�nimo de media.
            %       0 <= umbral_min < umbral_max
            obj.p.umbral_min = Parametro(double(20), 0, 360, 20);
            
            %   *umbral_max*: Valor m�ximo de media.
            %       umbral_min < umbral_max <= 360
            obj.p.umbral_max = Parametro(double(45), 0, 360, 37);
            
            %   *radio_erosion*: Radio en p�xeles de un disco con el que se
            %   erosiona la m�scara antes de calcular, ya que no hay buena
            %   informaci�n de color en los bordes.
            %       0 <= radio_erosion <= 50
            obj.p.radio_erosion = Parametro(int8(10), 0, 50, 10);
            
        end
        
    end
    
    
    %% M�todos protegidos
    methods(Access = protected)
        
        
        function [clase, internos] = clasificarResultados(obj, resultados)
            %CLASIFICARRESULTADOS Clasifica un Resultado
            % Uso: [clase, internos] = clasificarResultados(obj, resultados)
            % *clase*: Objeto de la clase Clases. Etiqueta.
            % *internos*: Estructura de informaci�n que emite la etapa
            %   clasificadora. Por ejemplo, valores que midi�, o im�genes �tiles
            %   para depuraci�n. En uso actualmente: campo 'valor', es la
            %   medici�n m�s importante que realiza la etapa. El descriptor.
            % *resultados*: Objeto de la clase Resultados
            
            % Estructura de resultados internos.
            internos = struct();
            
            % C�lculos en frente y dorso. Se puede usar
            %   mascara_frente o mascara_frente_tegumento.
            
            % Intentamos usar m�scara de tegumento. Si est� vac�a, se usa la del objeto.
            if any(resultados.mascara_frente_tegumento)
                mascara_frente = resultados.mascara_frente_tegumento;
            else
                mascara_frente = resultados.mascara_frente;
            end
               
            if any(resultados.mascara_dorso_tegumento)
                mascara_dorso = resultados.mascara_dorso_tegumento;
            else
                mascara_dorso = resultados.mascara_dorso;
            end
                
            % C�lculo
            [media_frente] = obj.clasificarImagen(resultados.imagen_frente, mascara_frente);
            [media_dorso] = obj.clasificarImagen(resultados.imagen_dorso, mascara_dorso);
            
            % La m�trica final es la media de los valores calculados en
            % frente y dorso.
            media_hue = mean([media_frente, media_dorso]);
            
            % Se guardan las medidas.
            internos.valor = media_hue;
            
            % Clasificaci�n
            if media_hue < obj.p.umbral_min.valor || media_hue > obj.p.umbral_max.valor
                clase = Clases.Mala;
            else
                clase = Clases.Primera;
            end
            
        end % segmentar1
        
        
    end % methods protected
    
    
    %% M�todos privados
    methods(Access = private)
        
        
        function [angulo_medio] = clasificarImagen(obj, imagen, mascara)
            %CLASIFICARIMAGEN
            % Uso: [desviacion] = clasificarImagen(obj, imagen, mascara)
            % *angulo_medio*: Media del color (canal H) en el objeto.
            % *imagen*: Imagen RGB.
            % *mascara*: Imagen binaria. Fondo negro, objeto en blanco.
            
            
            % Validaci�n de entradas
            % ToDo
            %- La mascara es bidimensional y l�gica
            validateattributes(mascara, {'logical'}, {'nonempty', 'ndims', 2}, ...
                'clasificarImagen', 'i_mascara', 2);
            
            
            % Erosi�n de la m�scara, porque en los bordes no hay
            % informaci�n de color confiable.
            SE = strel('disk', obj.p.radio_erosion.valor);
            mascara = imerode(mascara, SE);
            
            % Conversi�n a HSV
            hsv = rgb2hsv(imagen);
            h = hsv(:,:, 1);
            
            % Aplicaci�n de m�scara y normalizado a 360 grados
            h = 360 * h(mascara);
            
            % Conversi�n a vector
            h = h(:);
            
            
            % ESTAD�STICA DIRECCIONAL o ESTAD�STICA CIRCULAR
            % Conversi�n a coordenadas cartesianas, desde polares. Cada �ngulo
            % representa un vector de longitud unitaria. Las componentes, entonces, son
            % el coseno y el seno del �ngulo.
            [cartesianas_cos, cartesianas_sin] = pol2cart(deg2rad(h),ones(size(h)));
            
            % Se construye un vector R formado por la suma de cada uno de los �ngulos
            % iniciales. El �ngulo de R es el �ngulo medio de los vectores, mientras
            % que su m�dulo se utiliza para estimar una varianza.
            
            % Sumas de componentes
            suma_cos = sum(cartesianas_cos);
            suma_sin = sum(cartesianas_sin);
            
            % El �ngulo medio
            angulo_medio = rad2deg(atan2(suma_sin,suma_cos));
            
            % M�dulo y longitud media de los vectores, seg�n la longitud de R. Longitud
            % media: [0, 1]
            % longitud_vector = sqrt(suma_cos^2 + suma_sin^2);
            % longitud_media = longitud_vector / length(h);
            
            % Varianza [0, 1]
            %varianza = 1-longitud_media; % entre 0 y 1
            
            % Desviaci�n est�ndar [0, inf]
            %desviacion = sqrt(-2*log(longitud_media)); % entre 0 y inf
            
            % Conversi�n a grados
            %desviacion = desviacion*360/(2*pi);
            
        end % clasificarImagen
        
        
    end % Methods private
    
    
end