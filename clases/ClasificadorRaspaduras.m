classdef ClasificadorRaspaduras < Clasificador
    %CLASIFICADORRASPADURAS [ALMENDRAS] �rea sin tegumento
    %
    % Descriptor: �rea en mm2 que no tiene tegumento. Estimada a partir de
    % las zonas con poca saturaci�n en el canal S.
    %
    % Etiqueta seg�n par�metros variables.
    %
    % PARAMETROS
    %   *s_min*: Valor m�nimo de saturaci�n de un p�xel para ser
    %   considerado tegumento.
    %       0 <= s_min < 1
    %   *umbral_max_mm2*: L�mite de �rea sin tegumento.
    %   *factor_escala*: Factor multiplicativo de conversi�n px a mm. Se
    %   suponen p�xeles cuadrados.
    %       0 < factor_escala < 1000
    %   *radio_erosion_inicial*: Radio de un disco que erosiona la
    %   imagen, para no contemplar los bordes. Estos tienen una
    %   saturaci�n baja.
    %       0 <= radio_erosion_inicial <= 50
    %   *radio_apertura*: Radio de un disco de apertura luego de la
    %   umbralizaci�n, para cubrir m�s �rea sin tegumento.
    %       0 <= radio_apertura <= 50
    
    
    %% Propiedades p�blicas
    % Todos los par�metros que se puedan variar durante la ejecuci�n del
    % programa, probablemente a trav�s de la GUI.
    properties(SetAccess = public)
    end
    
    
    %% Propiedades privadas
    properties(SetAccess = private)
    end
    
    
    %% M�todos p�blicos
    methods(Access = public)
        
        
        function obj = ClasificadorRaspaduras(nombre, varargin)
            % Constructor
            
            % Validaci�n de entradas
            % ToDo
            
            % Constructor de superclase
            obj@Clasificador(nombre);
            
            
            % Par�metros
            % Par�metros variables de esta etapa. Son miembros de la
            % estructura p. Su formato:
            % Parametro(por_defecto, min, max, valor);
            
            %   *s_min*: Valor m�nimo de saturaci�n de un p�xel para ser
            %   considerado tegumento.
            %       0 <= s_min < 1
            obj.p.s_min = Parametro(double(0.84), 0, 1, 0.84);
            
            %   *umbral_max_mm2*: L�mite de �rea sin tegumento.
            maxmm2 = pi * (1.5)^2;
            obj.p.umbral_max_mm2 = Parametro(double(maxmm2), 0, 1000, maxmm2);
            
            %   *factor_escala*: Factor multiplicativo de conversi�n px a mm.
            %       0 < factor_escala < 1000
            fe = 1 / (26); % 1 px = 1/26 mm
            obj.p.factor_escala = Parametro(double(fe), 0, 1000, fe);
            
            %   *radio_erosion_inicial*: Radio de un disco que erosiona la
            %   imagen, para no contemplar los bordes. Estos tienen una
            %   saturaci�n baja.
            %       0 <= radio_erosion_inicial <= 50
            obj.p.radio_erosion_inicial = Parametro(int8(6), 0, 50, 6);
            
            %   *radio_apertura*: Radio de un disco de apertura luego de la
            %   umbralizaci�n, para cubrir m�s �rea sin tegumento.
            %       0 <= radio_apertura <= 50
            obj.p.radio_apertura = Parametro(int8(12), 0, 50, 12);
            
            
        end
        
    end
    
    
    %% M�todos protegidos
    methods(Access = protected)
        
        
        function [clase, internos] = clasificarResultados(obj, Resultados)
            %CLASIFICARRESULTADOS Clasifica un Resultado
            % Uso: [clase, internos] = clasificarResultados(obj, resultados)
            % *clase*: Objeto de la clase Clases. Etiqueta.
            % *internos*: Estructura de informaci�n que emite la etapa
            %   clasificadora. Por ejemplo, valores que midi�, o im�genes �tiles
            %   para depuraci�n. En uso actualmente: campo 'valor', es la
            %   medici�n m�s importante que realiza la etapa. El descriptor.
            % *resultados*: Objeto de la clase Resultados
            
            
            % Estructura de resultados internos.
            internos = struct();
            
            % C�lculos en frente y dorso
            [area_frente, internos.imagen_frente, mascara_frente_carne] = ...
                obj.clasificarImagen(Resultados.imagen_frente, Resultados.mascara_frente);
            [area_dorso, internos.imagen_dorso, mascara_dorso_carne] = ...
                obj.clasificarImagen(Resultados.imagen_dorso, Resultados.mascara_dorso);
            
            % Guardamos m�scaras de zonas con tegumento
            Resultados.mascara_frente_tegumento = ...
                logical(Resultados.mascara_frente-mascara_frente_carne);
            Resultados.mascara_dorso_tegumento = ...
                logical(Resultados.mascara_dorso-mascara_dorso_carne);
            
            % C�lculo de �rea total sin tegumento.
            % Factor de escala ^ 2 para �rea.
            area = (obj.p.factor_escala.valor^2) * (area_frente + area_dorso);
            
            % Se guarda la medida en la estructura de resultados internos.
            internos.valor = area;
            
            
            % Clasificaci�n
            if area > obj.p.umbral_max_mm2.valor
                clase = Clases.Mala;
            else
                clase = Clases.Primera;
                % Ac� se podr�a implementar Clases.Segunda
            end
            
        end
        
        
    end % methods protected
    
    
    %% M�todos privados
    methods(Access = private)
        
        
        function [area_raspaduras, img_auxiliar, mascara_carne] = ...
                clasificarImagen(obj, imagen, mascara)
            %CLASIFICARIMAGEN
            % Uso: [area_raspaduras, img_auxiliar, mascara_carne] = ...
            %    clasificarImagen(obj, imagen, mascara)
            % *area_raspaduras*: �rea en p�xeles de las zonas sin
            % tegumento, estimadas como las zonas con poca saturaci�n en el
            % canal S.
            % *img_auxiliar*: Fusi�n en falso color de la imagen original y
            % la m�scara de las raspaduras. �til para depuraci�n.
            % *mascara_carne*: M�scara con las zonas de raspaduras o
            % carne.
            % *imagen*: Imagen RGB.
            % *mascara*: Imagen binaria. Fondo negro, objeto en blanco.
            
            
            % Validaci�n de entradas
            %- La mascara es bidimensional y l�gica
            validateattributes(mascara, {'logical'}, {'nonempty', 'ndims', 2}, ...
                'clasificarImagen', 'i_mascara', 2);
            
            
            % Conversi�n a HSV
            hsv = rgb2hsv(imagen);
            s = hsv(:,:, 2);
            
            
            % M�scaras
            mascara_carne = mascara; % M�scara de raspaduras
            mascara_erosionada = []; % Auxiliar
            
            % Erosi�n de m�scara de raspaduras, para no contemplar los
            % bordes. Estos tienen una saturaci�n baja
            mascara_erosionada = imerode(mascara, strel('disk', obj.p.radio_erosion_inicial.valor));
            
            % Umbralizaci�n y binarizaci�n
            s(s > obj.p.s_min.valor) = 0;
            s = logical(s);
            
            % M�scara de solamente raspaduras (carne), exceptuando el borde
            % del objeto.
            mascara_carne = mascara_erosionada .* s;
            
            % Apertura
            mascara_carne = imopen(mascara_carne, strel('disk', obj.p.radio_apertura.valor));
            
            % Imagen auxiliar, �til para depuraci�n. Fusi�n en falso color
            % de la imagen original y la m�scara de carne.
            img_auxiliar = imfuse(imagen, mascara_carne, 'falsecolor');
            
            % C�lculos de �rea en p�xeles.
            area_raspaduras = sum(sum(mascara_carne));
            
        end % clasificarImagen
        
        
    end % Methods private
    
    
end