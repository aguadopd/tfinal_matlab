classdef PrincipalSet < Principal
    %PRINCIPALSET [ALMENDRAS] Para evaluar set de im�genes
    % Esta clase ejecuta el algoritmo de clasificaci�n sobre un conjunto de
    % im�genes previamente cargadas. Tambi�n eval�a los resultados
    % obtenidos, pudiendo generar archivos con las m�tricas.
    %
    % Las im�genes se cargan desde una tabla con el siguiente formato:
    % frente_original; frente_mascara; dorso_original; dorso_mascara; clase
    % Debe ser un archivo .CSV
    %
    % Los 4 primeros son nombres de archivo, **relativos a la ubicaci�n de
    % la lista**. 'clase' es la clase real de la almendra u objeto, y puede
    % ser 'Primera', 'Segunda', 'Mala', 'NoAlmendra' o 'Nada'.
    %
    % Si no hay informaci�n sobre la imagen del dorso, se usa la del frente
    % como si tambi�n fuese la del dorso. 
    %
    % Las im�genes de m�scara tampoco son obligatorias. Se usan para cargar
    % im�genes de los objetos retroiluminados, o m�scaras pregeneradas. 
    
    
    %% Propiedades p�blicas
    properties(SetAccess = public)
        % Arreglo tipo cell con los datos de los archivos a leer. Son los
        % de la tabla pero con las direcciones completas.
        datos = cell(0);
        
        % Arreglo tipo cell con los datos le�dos de la tabla.
        datos_originales;
        
        
        % Structs con m�tricas de evaluaci�n.
        
        %   Con todas las clases
        evaluacion1 = struct();
        %   % Aproximaci�n a 2 clases: Buenas (Primera+Segunda) y Malas
        %   (Mala, NoAlmendra, Nada)
        evaluacion2 = struct();
        %   % Aproximaci�n a 2 clases: Buenas (Primera) y Malas (Mala,
        %   NoAlmendra, Nada, Segunda)
        evaluacion3 = struct();
        
        % Matrices de confusi�n, en forma de tablas, para los 3 casos
        % mencionados anteriomente.
        C1 = table();
        C2 = table();
        C3 = table();
    end
    
    
    %% M�todos p�blicos
    methods(Access = public)
        
        
        function obj = PrincipalSet(nombre, dir_archivo_set)
            %PRINCIPALSET Constructor
            % Uso: PrincipalSet(nombre, dir_archivo_set)
            % *nombre*: String. Nombre para f�cil identificaci�n.
            % *dir_archivo_set*: String. Direcci�n a la tabla con los
            % archivos a cargar.
            %
            % Las im�genes se cargan desde una tabla con el siguiente
            % formato: frente_original; frente_mascara; dorso_original;
            % dorso_mascara; clase Debe ser un archivo .CSV
            %
            % Los 4 primeros son nombres de archivo, relativos a la
            % ubicaci�n de la lista. 'clase' es la clase real de la
            % almendra u objeto, y puede ser 'Primera', 'Segunda', 'Mala',
            % 'NoAlmendra' o 'Nada'.
            
            % Validaci�n de entradas
            % ToDo
            
            % Constructor de superclase
            obj@Principal(nombre);
            
            
            % LECTURA DE TABLA
            
            % Separaci�n de direcci�n, nombre_archivo, formato
            [direccion_tabla, ~, ~] = fileparts(dir_archivo_set);
            
            % Lectura como tabla. El formato es:
            % f_archivo_original; f_archivo_mascara; d_archivo_original; d_archivo_mascara; clase
            formato = '%s%s%s%s%s';
            delimitador = ';';
            T = readtable(dir_archivo_set, 'Delimiter', delimitador, ...
                'Format', formato);
            
            % Conversi�n de columnas a cells.
            fao = table2cell(T(:, 'f_archivo_original'));
            fam = table2cell(T(:, 'f_archivo_mascara'));
            dao = table2cell(T(:, 'd_archivo_original'));
            dam = table2cell(T(:, 'd_archivo_mascara'));
            c = table2cell(T(:, 'clase'));
            
            % Se guarda como arreglo cell. El miembro datos se modificar�
            % para que tenga las direcciones absolutas.
            obj.datos = cat(2, fao, fam, dao, dam, c);
            obj.datos_originales = obj.datos;
            
            % Completado de direcciones y copia de frente si no hay dorso.
            % Para todo el arreglo;
            for i = 1:size(obj.datos, 1)
                % Para frente y dorso, originales y m�scara;
                for j = 1:4
                    % Si no est� vac�a la celda
                    if ~isempty(obj.datos{i, j})
                        % Se completa el contenido con la ruta absoluta.
                        obj.datos{i, j} = fullfile(direccion_tabla, obj.datos{i, j});
                    end
                end
                % Copia del frente si no hay dorso.
                if isempty(obj.datos{i, 3})
                    obj.datos(i, 3:4) = obj.datos(i, 1:2);
                end
            end
            
        end % Constructor
        
        
        function resultados = procesarTodo(obj, flag_forzar)
            %PROCESARTODO Clasifica todo el conjunto de im�genes
            % Uso: resultados = procesarTodo(obj, flag_forzar)
            % *resultados*: Arreglo tipo cell de objetos de clase
            % Resultados.
            % *flag_forzar*: Booleano que determina si se ejecuta todo el
            % algoritmo o si se permite terminaci�n temprana en caso de que
            % el objeto ya sea de clase Mala o NoAlmendra.
            
            % Validaci�n de entradas
            % ToDo
            
            % Iteraci�n sobre todos los objetos
            n = size(obj.datos, 1);
            resultados = cell(n, 1);
            for i = 1:n
                img_frente = imread(obj.datos{i, 1});
                img_dorso = imread(obj.datos{i, 3});
                resultados{i} = obj.procesar(img_frente, img_dorso, i, flag_forzar);
            end
            
        end % procesarTodo
        
        
        function obj = evaluar(obj, resultados)
            %EVALUAR Eval�a los resultados obtenidos
            % Uso: evaluar(obj, resultados)
            % *resultados*: Arreglo tipo cell de objetos de clase
            % Resultados.
            % 
            % Genera m�tricas de evaluaci�n al comparar la clasificaci�n
            % obtenida contra las clases reales. Se almacenan en las
            % propiedades 'evaluacionX' y 'CX'.
            
            % Validaci�n de entradas.
            % ToDo
            
            % Cantidad de datos en la iteraci�n.
            n_datos = size(obj.datos, 1);
            
            % Debe haber tantos datos como elementos en el arreglo.
            if (n_datos ~= length(resultados))
                error('');
            end

            % Arreglos de etiquetas reales y estimadas.
            clases_reales = obj.datos(:, 5);
            clases_estimadas = cell(n_datos, 1);
                        
            % Carga de resultados y conversi�n a string.
            for i = 1:n_datos
                clases_estimadas{i} = char(resultados{i}.clase);
            end
            

            % ORDEN DE M�TRICAS DE MULTIC
            % Accuracy,
            % Precision (positive predictive value),
            % False discovery rate,
            % False omission rate,
            % Negative predictive value,
            % Prevalence,
            % Recall (hit rate, sensitivity, true positive rate),
            % False positive rate (fall-out),
            % Positive likelihood ratio,
            % False negative rate (miss rate),
            % True negative rate (specificity),
            % Negative likelihood ratio,
            % Diagnostic odds ratio,
            % Informedness,
            % Markedness,
            % F-score,
            % G-measure,
            % Matthews correlation coefficient.
            
            
            % Evaluaci�n considerando todas las clases.
            % Las mediciones son un promedio de las mediciones intraclase,
            % que se est�n en N y se obtienen haciendo uno contra todos
            % para cada clase.
            [C, T, D, M, N, ORDER] = multic(clases_reales, clases_estimadas);
            
            % Matriz de confusi�n.
            obj.C1 = array2table(C, 'VariableNames', ORDER, 'RowNames', ORDER);
            
            % Recuento
            if any(strcmp(ORDER, 'nada'))
                idxnada = find(strcmp(ORDER, 'nada'));
                obj.evaluacion1.nada = sum(C(idxnada,:));
            end
            if any(strcmp(ORDER, 'mala'))
                idxmala = find(strcmp(ORDER, 'mala'));
                obj.evaluacion1.mala = sum(C(idxmala,:));
            end
            if any(strcmp(ORDER, 'noalmendra'))
                idxnoalmendra = find(strcmp(ORDER, 'noalmendra'));
                obj.evaluacion1.noalmendra = sum(C(idxnoalmendra,:));
            end
            if any(strcmp(ORDER, 'primera'))
                idxprimera = find(strcmp(ORDER, 'primera'));
                obj.evaluacion1.primera = sum(C(idxprimera,:));
            end
            if any(strcmp(ORDER, 'segunda'))
                idxsegunda = find(strcmp(ORDER, 'segunda'));
                obj.evaluacion1.segunda = sum(C(idxsegunda,:));
            end
            
            % Medidas
            obj.evaluacion1.exactitud = D(1);
            obj.evaluacion1.precision = D(2);
            %             obj.evaluacion1.recuperacion = D(7);
            %             obj.evaluacion1.f1 = D(16);
            obj.evaluacion1.correctas = sum(sum(C.*eye(size(C))));
            obj.evaluacion1.incorrectas = n_datos - obj.evaluacion1.correctas;
            obj.evaluacion1.n = n_datos;
            
            
            % Aproximaci�n a 2 clases: Buenas (Primera+Segunda) y Malas (Mala, NoAlmendra, Nada)
            clases_reales2 = clases_reales;
            clases_estimadas2 = clases_estimadas;
            clases_reales2(strcmp('Primera', clases_reales)) = {'B'};
            clases_reales2(strcmp('Segunda', clases_reales)) = {'B'};
            clases_reales2(strcmp('Mala', clases_reales)) = {'M'};
            clases_reales2(strcmp('NoAlmendra', clases_reales)) = {'M'};
            clases_reales2(strcmp('Nada', clases_reales)) = {'M'};
            clases_estimadas2(strcmp('Primera', clases_estimadas)) = {'B'};
            clases_estimadas2(strcmp('Segunda', clases_estimadas)) = {'B'};
            clases_estimadas2(strcmp('Mala', clases_estimadas)) = {'M'};
            clases_estimadas2(strcmp('NoAlmendra', clases_estimadas)) = {'M'};
            clases_estimadas2(strcmp('Nada', clases_estimadas)) = {'M'};
            
            % C�lculo.
            [C2, T2, D2, M2, N2, ORDER2] = multic(clases_reales2, clases_estimadas2);
            
            % Matriz de confusi�n.
            obj.C2 = array2table(C2, 'VariableNames', ORDER2, 'RowNames', ORDER2);
            
            % Recuento.
            if any(strcmp(ORDER2, 'm'))
                idxm = find(strcmp(ORDER2, 'm'));
                obj.evaluacion2.m = sum(C2(idxm,:));
            end
            if any(strcmp(ORDER2, 'b'))
                idxb = find(strcmp(ORDER2, 'b'));
                obj.evaluacion2.b = sum(C2(idxb,:));
            end
            
            % Medidas.
            obj.evaluacion2.exactitud = D2(1);
            obj.evaluacion2.precision = D2(2);
            %             obj.evaluacion2.recuperacion = D2(7);
            %             obj.evaluacion2.f1 = D2(16);
            obj.evaluacion2.correctas = sum(sum(C2.*eye(size(C2))));
            obj.evaluacion2.incorrectas = n_datos - obj.evaluacion2.correctas;
            obj.evaluacion2.n = n_datos;
            
            
            % Aproximaci�n a 2 clases: Buenas (Primera) y Malas (Mala, NoAlmendra, Nada, Segunda)
            clases_reales3 = clases_reales;
            clases_estimadas3 = clases_estimadas;
            clases_reales3(strcmp('Primera', clases_reales)) = {'B'};
            clases_reales3(strcmp('Segunda', clases_reales)) = {'M'};
            clases_reales3(strcmp('Mala', clases_reales)) = {'M'};
            clases_reales3(strcmp('NoAlmendra', clases_reales)) = {'M'};
            clases_reales3(strcmp('Nada', clases_reales)) = {'M'};
            clases_estimadas3(strcmp('Primera', clases_estimadas)) = {'B'};
            clases_estimadas3(strcmp('Segunda', clases_estimadas)) = {'M'};
            clases_estimadas3(strcmp('Mala', clases_estimadas)) = {'M'};
            clases_estimadas3(strcmp('NoAlmendra', clases_estimadas)) = {'M'};
            clases_estimadas3(strcmp('Nada', clases_estimadas)) = {'M'};
            
            % C�lculo
            [C3, T3, D3, M3, N3, ORDER3] = multic(clases_reales3, clases_estimadas3);
            
            % Matriz de confusi�n
            obj.C3 = array2table(C3, 'VariableNames', ORDER3, 'RowNames', ORDER3);
            
            % Recuento.
            if any(strcmp(ORDER3, 'm'))
                idxm = find(strcmp(ORDER3, 'm'));
                obj.evaluacion3.m = sum(C3(idxm,:));
            end
            if any(strcmp(ORDER3, 'b'))
                idxb = find(strcmp(ORDER3, 'b'));
                obj.evaluacion3.b = sum(C3(idxb,:));
            end
            
            % Medidas
            obj.evaluacion3.exactitud = D3(1);
            obj.evaluacion3.precision = D3(2);
            %             obj.evaluacion3.recuperacion = D3(7);
            %             obj.evaluacion3.f1 = D3(16);
            obj.evaluacion3.correctas = sum(sum(C3.*eye(size(C3))));
            obj.evaluacion3.incorrectas = n_datos - obj.evaluacion3.correctas;
            obj.evaluacion3.n = n_datos;            
            
        end
        
        
        function guardarTablas(obj, archivo)
            %GUARDARTABLAS
            % Uso: guardarTablas(obj, archivo)
            % *archivo*: String con nombre del archivo en donde se
            % guardaran los resultados de la evaluaci�n.
            % Se usan hojas, as� que debe ser formato .XLSX o .XLS
            %
            % Ej: PS.guardarTablas('evaluacion.xls');
            
            % Validaci�n de entradas
            % ToDo
            
            % Se elimina el archivo, si existe.
            delete(archivo);
            
            % Escritura en 3 hojas.
            T = struct2table(obj.evaluacion1);
            writetable(T, archivo, 'Sheet', 'ev1')
            writetable(obj.C1, archivo, 'Sheet', 'ev1', 'Range', 'A5', 'WriteRowNames', true);
            T = struct2table(obj.evaluacion2);
            writetable(T, archivo, 'Sheet', 'ev2');
            writetable(obj.C2, archivo, 'Sheet', 'ev2', 'Range', 'A5', 'WriteRowNames', true);
            T = struct2table(obj.evaluacion3);
            writetable(T, archivo, 'Sheet', 'ev3');
            writetable(obj.C3, archivo, 'Sheet', 'ev3', 'Range', 'A5', 'WriteRowNames', true);            
            
        end % guardarTablas
        
    end % M�todos p�blicos
    
end % classdef