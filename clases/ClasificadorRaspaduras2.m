classdef ClasificadorRaspaduras2 < Clasificador
    %CLASIFICADORRASPADURAS2 [ALMENDRAS] �rea sin tegumento
    %
    % Descriptor: �rea que no tiene tegumento, en mm2. Estimada a partir de
    % umbrales manuales en los canales H, S y V, que luego se multiplican.
    %
    % Etiqueta seg�n par�metros variables.
    %
    % PARAMETROS
    %   *h_min*: Matiz m�nimo.
    %       0 <= h_min < h_max
    %   *h_max*: Matiz m�ximo
    %       h_min < h_max <= 1
    %   *s_min*: Saturaci�n m�nima.
    %       0<= s_min < s_max
    %   *s_max*: Saturaci�n m�xima.
    %       s_min < s_max < 1
    %   *v_min*: Valor m�nimo.
    %       0 <= v_min < v_max
    %   *v_max*: Valor m�ximo
    %       v_min < v_max <= 1
    %   *umbral_max_mm2*: L�mite de �rea sin tegumento.
    %   *factor_escala*: Factor multiplicativo de conversi�n px a mm. Se
    %   suponen p�xeles cuadrados.
    %       0 < factor_escala < 1000
    %   *radio_erosion_inicial*: Radio de un disco que erosiona la
    %   imagen, para no contemplar los bordes; estos tienen mala
    %   informaci�n de color.
    %       0 <= radio_erosion_inicial <= 50
    %   *radio_apertura*: Radio de un disco de apertura luego de la
    %   umbralizaci�n, para cubrir m�s �rea sin tegumento.
    %       0 <= radio_apertura <= 50
    
    
    %% Propiedades p�blicas
    % Todos los par�metros que se puedan variar durante la ejecuci�n del
    % programa, probablemente a trav�s de la GUI.
    properties(SetAccess = public)
    end
    
    
    %% Propiedades privadas
    properties(SetAccess = private)
    end
    
    
    %% M�todos p�blicos
    methods(Access = public)
        
        
        function obj = ClasificadorRaspaduras2(nombre, varargin)
            % Constructor
            
            % Validaci�n de entradas
            % ToDo
            
            % Constructor de superclase
            obj@Clasificador(nombre);
            
            
            % Par�metros
            % Par�metros variables de esta etapa. Son miembros de la
            % estructura p. Su formato:
            % Parametro(por_defecto, min, max, valor);
            
            %   *h_min*: Matiz m�nimo.
            %       0 <= h_min < h_max
            obj.p.h_min = Parametro(double(18*360/255), 0, 360, double(18*360/255));
            
            %   *h_max*: Matiz m�ximo
            %       h_min < h_max <= 360
            obj.p.h_max = Parametro(double(74*360/255), 0, 360, double(74*360/255));
            
            %   *s_min*: Saturaci�n m�nima.
            %       0<= s_min < s_max
            obj.p.s_min = Parametro(double(0), 0, 1, 0);
            
            %   *s_max*: Saturaci�n m�xima.
            %       s_min < s_max < 1
            obj.p.s_max = Parametro(double(220/255), 0, 1, 220/255);
            
            %   *v_min*: Valor m�nimo.
            %       0 <= v_min < v_max
            obj.p.v_min = Parametro(double(47/255), 0, 1, 47/255);
            
            %   *v_max*: Valor m�ximo
            %       v_min < v_max <= 1
            obj.p.v_max = Parametro(double(255/255), 0, 1, 255/255);
            
            %   *umbral_max_mm2*: L�mite de �rea sin tegumento.
            maxmm2 = pi * (1.5)^2;
            obj.p.umbral_max_mm2 = Parametro(double(maxmm2), 0, 1000, maxmm2);
            
            %   *factor_escala*: Factor multiplicativo de conversi�n px a mm.
            %       0 < factor_escala < 1000
            fe = 1 / (26); % 1 px = 1/26 mm
            obj.p.factor_escala = Parametro(double(fe), 0, 1000, fe);
            
            %   *radio_erosion_inicial*: Radio de un disco que erosiona la
            %   imagen, para no contemplar los bordes. Estos tienen una
            %   saturaci�n baja.
            %       0 <= radio_erosion_inicial <= 50
            obj.p.radio_erosion_inicial = Parametro(int8(2), 0, 50, 2);
            
            %   *radio_apertura*: Radio de un disco de apertura luego de la
            %   umbralizaci�n, para cubrir m�s �rea sin tegumento.
            %       0 <= radio_apertura <= 50
            obj.p.radio_apertura = Parametro(int8(7), 0, 50, 7);
            
            
        end
        
    end
    
    
    %% M�todos protegidos
    methods(Access = protected)
        
        
        function [clase, internos] = clasificarResultados(obj, Resultados)
            %CLASIFICARRESULTADOS Clasifica un Resultado
            % Uso: [clase, internos] = clasificarResultados(obj, resultados)
            % *clase*: Objeto de la clase Clases. Etiqueta.
            % *internos*: Estructura de informaci�n que emite la etapa
            %   clasificadora. Por ejemplo, valores que midi�, o im�genes �tiles
            %   para depuraci�n. En uso actualmente: campo 'valor', es la
            %   medici�n m�s importante que realiza la etapa. El descriptor.
            % *resultados*: Objeto de la clase Resultados
            
            
            % Estructura de resultados internos.
            internos = struct();
            
            % C�lculos en frente y dorso
            [area_frente, internos.imagen_frente, mascara_frente_carne] = ...
                obj.clasificarImagen(Resultados.imagen_frente, Resultados.mascara_frente);
            [area_dorso, internos.imagen_dorso, mascara_dorso_carne] = ...
                obj.clasificarImagen(Resultados.imagen_dorso, Resultados.mascara_dorso);
            
            % Guardamos m�scaras de zonas con tegumento
            Resultados.mascara_frente_tegumento = ...
                logical(Resultados.mascara_frente-mascara_frente_carne);
            Resultados.mascara_dorso_tegumento = ...
                logical(Resultados.mascara_dorso-mascara_dorso_carne);
            
            % C�lculo de �rea total sin tegumento.
            % Factor de escala ^ 2 para �rea.
            area = (obj.p.factor_escala.valor^2) * (area_frente + area_dorso);
            %             area = area_frente;
            
            % Se guarda la medida en la estructura de resultados internos.
            internos.valor = area;
            
            
            % Clasificaci�n
            if area > obj.p.umbral_max_mm2.valor
                clase = Clases.Mala;
            else
                clase = Clases.Primera;
                % Ac� se podr�a implementar Clases.Segunda
            end
            
        end
        
        
    end % methods protected
    
    
    %% M�todos privados
    methods(Access = private)
        
        
        function [area_raspaduras, img_auxiliar, mascara_raspaduras] = ...
                clasificarImagen(obj, imagen, mascara)
            %CLASIFICARIMAGEN
            % Uso: [area_raspaduras, img_auxiliar, mascara_raspaduras] = ...
            %    clasificarImagen(obj, imagen, mascara)
            % *area_raspaduras*: �rea en p�xeles de las zonas sin
            % tegumento, estimadas como las zonas con poca saturaci�n en el
            % canal S y adem�s zonas de cierto matiz en el canal H y adem�s
            % zonas de cierto valor en el canal V.
            % *img_auxiliar*: Fusi�n en falso color de la imagen original y
            % la m�scara de las raspaduras. �til para depuraci�n.
            % *mascara_raspaduras*: M�scara con las zonas de raspaduras o
            % carne.
            % *imagen*: Imagen RGB.
            % *mascara*: Imagen binaria. Fondo negro, objeto en blanco.
            
            
            % Validaci�n de entradas
            %- La mascara es bidimensional y l�gica
            validateattributes(mascara, {'logical'}, {'nonempty', 'ndims', 2}, ...
                'clasificarImagen', 'i_mascara', 2);
            
            
            % Conversi�n a HSV
            hsv = rgb2hsv(imagen);
            h = hsv(:,:, 1);
            s = hsv(:,:, 2);
            v = hsv(:,:, 3);
            
            
            % M�scaras
            hm = mascara;
            sm = mascara;
            vm = mascara;
            
            mascara2 = mascara; % M�scara de raspaduras
            mascara3 = []; % Auxiliar
            
            % Erosi�n de m�scara de raspaduras, para no contemplar los
            % bordes. Estos tienen poca informaci�n de color
            mascara3 = imerode(mascara2, strel('disk', obj.p.radio_erosion_inicial.valor));
            
            
            % Umbralizaci�n
            hm(h < obj.p.h_min.valor/360) = 0;
            hm(h > obj.p.h_max.valor/360) = 0;
            
            sm(s < obj.p.s_min.valor) = 0;
            sm(s > obj.p.s_max.valor) = 0;
            
            vm(v < obj.p.v_min.valor) = 0;
            vm(v > obj.p.v_max.valor) = 0;
            
            
            % M�scara
            mascara2 = mascara3 .* hm .* sm .* vm;
                
            % Apertura
            mascara2 = imopen(mascara2, strel('disk', obj.p.radio_apertura.valor));
            
            % Se guarda como l�gica
            mascara_raspaduras = logical(mascara2);
            
            % Imagen auxiliar, �til para depuraci�n. Fusi�n en falso color
            % de la imagen original y la m�scara de carne.
            img_auxiliar = imfuse(imagen, mascara2, 'falsecolor');
            
            % C�lculos de �rea en p�xeles.
            area_raspaduras = sum(sum(mascara2));
            
        end % clasificarImagen
        
        
    end % Methods private
    
    
end