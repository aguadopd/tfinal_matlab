classdef ClasificadorExtensionBB < Clasificador
    %CLASIFICADOREXTENSIONBB [ALMENDRAS] Extensi�n respecto a caja
    %
    % Descriptor: Extensi�n = �rea_objeto/�rea_caja_envolvente.
    % La caja envolvente o bounding box es calculada luego de
    % horizontalizar la imagen seg�n el �ngulo que determine la elipse que
    % tenga los mismos segundos momentos que el objeto. Esto es para tener
    % la caja de menor �rea posible, ya que la caja siempre se se calcula
    % horizontal.
    %
    % Este clasificador busca eliminar objetos que tengan ramificaciones o
    % que ocupen poco espacio de su caja envolvente. Ejemplo: ramas.
    %
    % Se supone que, para clasificar por forma, es indistinto usar
    % el frente o el dorso. Usamos el frente.
    %
    % Etiqueta seg�n par�metros variables. Si la relaci�n de aspecto se
    % encuentra fuera de un rango determinado, se considera que el objeto
    % es NoAlmendra.
    %
    % PARAMETROS
    %   *min_extension*: Extensi�n m�nima para ser considerado almendra.
    %       0 <= min_extension <= 1
    
    
    %% Propiedades p�blicas
    % Todos los par�metros que se puedan variar durante la ejecuci�n del
    % programa, probablemente a trav�s de la GUI.
    properties(SetAccess = public)
    end
    
    
    %% Propiedades privadas
    properties(SetAccess = private)
    end
    
    
    %% M�todos p�blicos
    methods(Access = public)
        
        
        function obj = ClasificadorExtensionBB(nombre, varargin)
            % Constructor
            
            % Validaci�n de entradas
            % ToDO
            
            % Constructor de superclase
            obj@Clasificador(nombre);
            
            
            % Par�metros
            % Par�metros variables de esta etapa. Son miembros de la
            % estructura p. Su formato:
            % Parametro(por_defecto, min, max, valor);
            
            %   *min_extension*: Extensi�n m�nima para ser considerado almendra.
            %       0 <= min_extension <= 1
            obj.p.min_extension = Parametro(double(0.7), 0, 1, 0.7);
            
        end
        
    end
    
    
    %% M�todos protegidos
    methods(Access = protected)
        
        
        function [clase, internos] = clasificarResultados(obj, resultados)
            %CLASIFICARRESULTADOS Clasifica un Resultado
            % Uso: [clase, internos] = clasificarResultados(obj, resultados)
            % *clase*: Objeto de la clase Clases. Etiqueta.
            % *internos*: Estructura de informaci�n que emite la etapa
            %   clasificadora. Por ejemplo, valores que midi�, o im�genes �tiles
            %   para depuraci�n. En uso actualmente: campo 'valor', es la
            %   medici�n m�s importante que realiza la etapa. El descriptor.
            % *resultados*: Objeto de la clase Resultados
            
            % Estructura de resultados internos.
            internos = struct();
            
            % Suponemos que, para clasificar por forma, es indistinto usar
            % el frente o el dorso. Usamos el frente.
            [extension] = obj.clasificarImagen(resultados.mascara_frente);
            
            % Se guarda el valor en la estructura de resultados internos.
            internos.valor = extension;
            
            % Etiqueta seg�n par�metro variable.
            if extension < obj.p.min_extension.valor
                clase = Clases.NoAlmendra;
            else
                clase = Clases.Primera;
            end
            
        end
        
        
    end % methods protected
    
    
    %% M�todos privados
    methods(Access = private)
        
        
        function [extension] = clasificarImagen(obj, mascara)
            %CLASIFICARIMAGEN Clasifica una imagen de m�scara
            % Uso: [extension] = clasificarImagen(obj, mascara)
            % *extension*: �rea_objeto/�rea_caja_envolvente.
            % La caja envolvente o bounding box es calculada luego de
            % horizontalizar la imagen seg�n el �ngulo que determine la elipse que
            % tenga los mismos segundos momentos que el objeto. Esto es para tener
            % la caja de menor �rea posible, ya que la caja siempre se se calcula
            % horizontal.
            % *mascara*: Imagen binaria. Fondo negro, objeto en blanco.
            
            
            % Validaci�n de entradas
            %- La mascara es bidimensional y l�gica
            validateattributes(mascara, {'logical'}, {'nonempty', 'ndims', 2}, ...
                'clasificarImagen', 'i_mascara', 1);
            
            
            % ROTACI�N
            % C�lculo de la orientaci�n de las regiones conectadas en la
            % m�scara.
            propiedades = regionprops(mascara, 'Orientation');
            
            % Suponemos que s�lo hay una regi�n conectada ---solamente 1
            % objeto. Por las dudas, usamos la primera.
            propiedades = propiedades(1);
            
            % Rotaci�n seg�n el �ngulo entre el eje horizontal y el eje
            % mayor de la elipse ajustada con los mismos momentos de
            % segundo orden que la regi�n. Rotamos para tener la menor
            % bounding box posible. Queda horizontal.
            tita = deg2rad(-propiedades.Orientation);
            mascara_rotada = imrotate(mascara, rad2deg(tita));
            
            
            % Extensi�n
            % Rec�lculo de propiedades de las regiones conectadas.
            propiedades_rotada = regionprops(mascara_rotada, 'Extent');
            
            extension = propiedades_rotada.Extent;
            
        end % clasificarImagen
        
        
    end % Methods private
    
    
end