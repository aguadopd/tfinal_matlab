classdef Principal < matlab.mixin.Copyable
    %PRINCIPAL [ALMENDRAS] Clase principal
    % Esta clase es la que almacena el algoritmo de procesamiento y lo hace
    % funcionar sobre las im�genes u objetos de clase Resultado.
    %
    % Puede procesar im�genes (a partir de las cu�les crea un objeto de
    % clase Resultados) o un objeto Resultados directamente.
    
    
    %% Propiedades p�blicas
    properties(SetAccess = public)
        % Cell de etapas. Cada elemento es un Preprocesador, Segmentador o
        % Clasificador. El orden es importante porque se ejecutar�n
        % secuencialmente en el mismo orden.
        algoritmo = cell(0);
    end
    
    
    %% Propiedades privadas
    properties(SetAccess = private)
        % Nombre para f�cil identificaci�n.
        nombre = '';
    end
    
    
    %% M�todos p�blicos
    methods(Access = public)
        
        
        function obj = Principal(i_nombre, varargin)
            % Constructor
            
            % ToDo: Validaci�n de entradas.
            
            % Inicializaci�n
            obj.nombre = i_nombre;
        end
        
        
        function obj = agregar(obj, i_componente)
            %AGREGAR Agregar etapa al algoritmo
            
            % Validaci�n de entradas
            % ToDo
            % Es Segmentador o Clasificador o Preprocesador
            
            obj.algoritmo{end+1} = i_componente;
        end
        
        
        function resultado = procesar(obj, imagen_frente, imagen_dorso, id_numero, flag_forzar)
            %PROCESAR Procesar im�genes
            % Uso: resultado = procesar(obj, imagen_frente, imagen_dorso, id_numero, flag_forzar)
            % *resultado*: Objeto de la clase Resultados.
            % *imagen_frente*: Imagen RGB del frente del objeto.
            % *imagen_dorso*: Imagen RGB del dorso del objeto.
            % *id_numero*: N�mero de identificaci�n. Auxiliar.
            % *flag_forzar*: Booleano para forzar la ejecuci�n de todas las
            % etapas, independientemente de si el objeto ya fue
            % descartado/rechazado.
            %
            % Esta funci�n crea un objeto de clase Resultados a partir de
            % im�genes de frente y dorso. Se puede asignar al objeto un
            % n�mero para f�cil identificaci�n. Luego ejecuta el algoritmo
            % sobre el 'resultado', para clasificarlo. Normalmente, la
            % clasificaci�n deber� terminar una vez que el objeto fue
            % rechazado. El booleano flag_forzar en true ejecutar� todas
            % las etapas del algoritmo, sin importar la clase asignada.
            
            % Validaci�n de entradas
            % ToDo
            
            % Creaci�n del objeto de clase Resultados
            resultado = Resultados(imagen_frente, imagen_dorso, id_numero);
            
            % Procesamiento
            for i = 1:length(obj.algoritmo)
                if isa(obj.algoritmo{i}, 'Preprocesador')
                    resultado = obj.algoritmo{i}.procesar(resultado);
                elseif isa(obj.algoritmo{i}, 'Segmentador')
                    resultado = obj.algoritmo{i}.segmentar(resultado);
                elseif isa(obj.algoritmo{i}, 'Clasificador')
                    resultado = obj.algoritmo{i}.clasificar(resultado, flag_forzar);
                end
            end
            
        end
        
        
        function resultado = procesarResultado(obj, i_resultado, flag_forzar)
            %PROCESARRESULTADO Reprocesar un resultado
            % Uso: resultado = procesarResultado(obj, i_resultado, flag_forzar)
            % *resultado*: Objeto de la clase Resultados.
            % *i_resultado*: Objeto de la clase Resultados.
            % *flag_forzar*: Booleano para forzar la ejecuci�n de todo el
            % algoritmo, sin terminaci�n temmprana.
            %
            % Esta funci�n sirve para ejecutar el algoritmo sobre un objeto
            % de clase Resultados existente, y no desde im�genes. Se usa en
            % la GUI.
            
            % Validaci�n de entradas.
            % ToDo
            
            % Creaci�n del nuevo objeto resultados, a partir del existente.
            % Procesamiento.
            img_f = i_resultado.imagen_original_frente;
            img_d = i_resultado.imagen_original_dorso;
            id_numero = i_resultado.id_numero;
            resultado = obj.procesar(img_f, img_d, id_numero, flag_forzar);
            
        end
        
        
    end
    
    
end