classdef Parametro
    %PARAMETRO [ALMENDRAS] Clase Par�metro
    % Esta clase sirve para contener toda variable modificable o ajustable
    % de los Preprocesadores y Clasificadores. Define:
    %   * valor: Valor actual.
    %   * default: Valor por defecto.
    %   * min: Valor m�nimo.
    %   * max: Valor m�ximo.
    %
    % ToDo: Implementar setters para asegurar que los valores ingresados
    % est�n dentro del rango [min, max] y que sean del mismo tipo que
    % 'default'. No dar error, sino warning y ajustar al rango e intentar
    % castear al tipo correspondiente.
    
    
    %% Propiedades p�blicas
    % Todos los par�metros que se puedan variar durante la ejecuci�n del
    % programa, probablemente a trav�s de la GUI.
    properties
        % Valor actual.
        valor
        
        % Valor por defecto. Establece el tipo.
        default
        
        % Valor m�nimo.
        min
        
        % Valor m�ximo.
        max
    end
    
    
    %% M�todos p�blicos
    methods
        
        
        function obj = Parametro(i_default, i_min, i_max, i_valor)
            % PARAMETRO Constructor
            % Uso: Parametro( i_default, i_min, i_max, i_valor )
            
            
            obj.default = i_default;
            
            % ToDo: Verificaci�n de tipos y rango.
            obj.min = i_min;
            obj.max = i_max;
            obj.valor = i_valor;
            
        end
        
        
        % Faltan setters.
        % ToDo.
        
        
    end
    
end