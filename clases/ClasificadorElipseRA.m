classdef ClasificadorElipseRA < Clasificador
    %CLASIFICADORELIPSERA [ALMENDRAS] Relaci�n de aspecto elipse
    %
    % Descriptor: Relaci�n entre los radios mayor y menor de la elipse que
    % tiene los mismos segundos momentos que la m�scara del objeto.
    %
    % Este clasificador busca eliminar objetos que no tengan las
    % proporciones de una almendra buena. Puede complementar a la relaci�n
    % de aspecto de la caja envolvente (bounding box).
    %
    % Etiqueta seg�n par�metros variables. Si la relaci�n de aspecto se
    % encuentra fuera de un rango determinado, se considera que el objeto
    % es NoAlmendra.
    %
    % PARAMETROS
    %   *umbral_min*: Relaci�n de aspecto m�nima para ser considerado
    %   almendra.
    %       0 <= umbral_min < umbral_max
    %   *umbral_max*: Relaci�n de aspecto m�xima para ser considerado
    %   almendra.
    %       umbral_min < umbral_max <= 1
    
    
    %% Propiedades p�blicas
    % Todos los par�metros que se puedan variar durante la ejecuci�n del
    % programa, probablemente a trav�s de la GUI.
    properties(SetAccess = public)
    end
    
    
    %% Propiedades privadas
    properties(SetAccess = private)
    end
    
    
    %% M�todos p�blicos
    methods(Access = public)
        
        
        function obj = ClasificadorElipseRA(nombre, varargin)
            % Constructor
            
            % Validaci�n de entradas
            % ToDo
            
            % Constructor de superclase
            obj@Clasificador(nombre);
            
            
            % Par�metros
            % Par�metros variables de esta etapa. Son miembros de la
            % estructura p. Su formato:
            % Parametro(por_defecto, min, max, valor);
            
            %   *umbral_min*: Relaci�n de aspecto m�nima para ser considerado
            %   almendra.
            %       0 <= umbral_min < umbral_max
            obj.p.umbral_min = Parametro(double(0.45), 0, 1, 0.45);
            
            %   *umbral_max*: Relaci�n de aspecto m�xima para ser considerado
            %   almendra.
            %       umbral_min < umbral_max <= 1
            obj.p.umbral_max = Parametro(double(0.7), 0, 1, 0.69);
            
        end
        
        
    end
    
    
    %% M�todos protegidos
    methods(Access = protected)
        
        
        function [clase, internos] = clasificarResultados(obj, resultados)
            %CLASIFICARRESULTADOS Clasifica un Resultado
            % Uso: [clase, internos] = clasificarResultados(obj, resultados)
            % *clase*: Objeto de la clase Clases. Etiqueta.
            % *internos*: Estructura de informaci�n que emite la etapa
            %   clasificadora. Por ejemplo, valores que midi�, o im�genes �tiles
            %   para depuraci�n. En uso actualmente: campo 'valor', es la
            %   medici�n m�s importante que realiza la etapa. El descriptor.
            % *resultados*: Objeto de la clase Resultados
            
            % Estructura de resultados internos.
            internos = struct();
            
            % Clasificaci�n de frente y dorso.
            [ra_frente, internos.imagen_frente] = obj.clasificarImagen(resultados.mascara_frente);
            [ra_dorso, internos.imagen_dorso] = obj.clasificarImagen(resultados.mascara_dorso);
            
            % La m�trica final es la media.
            relacion_aspecto = mean([ra_frente, ra_dorso]);
            
            % Se guarda el valor en la estructura de resultados internos.
            internos.valor = relacion_aspecto;
            
            % Etiqueta seg�n par�metros variables.
            if relacion_aspecto < obj.p.umbral_min.valor || ...
                    relacion_aspecto > obj.p.umbral_max.valor
                clase = Clases.NoAlmendra;
            else
                clase = Clases.Primera;
            end
            
        end
        
        
    end % methods protected
    
    
    %% M�todos privados
    methods(Access = private)
        
        
        function [relacion_aspecto_elipse, img_interna] = clasificarImagen(obj, mascara)
            %CLASIFICARIMAGEN Clasifica una imagen de m�scara
            % Uso: [relacion_aspecto_elipse, img_interna] = clasificarImagen(obj, mascara)
            % *relacion_aspecto_elipse*: eje_mayor/eje_menor de elipse de
            % mismos segundos momentos que el objeto.
            % *img_interna*: Una im�gen de la m�scara, con dibujos de la
            % elipse estimada y sus ejes. �til para depuraci�n.
            % *mascara*: Imagen binaria. Fondo negro, objeto en blanco.
            
            
            % Validaci�n de entradas
            %- La mascara es bidimensional y l�gica
            validateattributes(mascara, {'logical'}, {'nonempty', 'ndims', 2}, ...
                'clasificarImagen', 'i_mascara', 1);
            
            % C�LCULO
            % C�lculo de propiedades de las regiones conectadas.
            % 'Centroid' y 'Orientation' son necesarias para la creaci�n de
            % la imagen auxiliar.
            propiedades = regionprops(mascara, 'Centroid', 'MinorAxisLength', ...
                'MajorAxisLength', 'Orientation');
            
            % Se supone que s�lo hay una regi�n conectada ---solamente 1
            % objeto. Por las dudas, usamos la primera.
            propiedades = propiedades(1);
            
            % Longitud ejes
            MAl = propiedades.MajorAxisLength;
            mAl = propiedades.MinorAxisLength;
            
            relacion_aspecto_elipse = mAl / MAl; % Eje menor / Eje mayor
            
            
            % IMAGEN AUXILIAR
            % Extra para gr�fica. Ojo, puede ser pesado.
            % Coordenadas con eje centrado y rotado.
            img_interna = [];
%             MAx1 = -MAl / 2;
%             MAx2 = -MAx1;
%             MAy1 = 0;
%             MAy2 = 0;
%             mAx1 = 0;
%             mAx2 = 0;
%             mAy1 = mAl / 2;
%             mAy2 = -mAy1;
%             
%             % Matriz de coordenadas de los 4 puntos, 2 por eje
%             P = [MAx1, MAy1; MAx2, MAy2; mAx1, mAy1; mAx2, mAy2]';
%             
%             % Matriz de rotataci�n
%             tita = deg2rad(-propiedades.Orientation);
%             R = [cos(tita), -sin(tita); sin(tita), cos(tita)];
%             
%             % Rotamos puntos
%             PP = R * P;
%             
%             % Matriz de centro, para desplazar
%             C = propiedades.Centroid';
%             C = repmat(C, [1, 4]);
%             
%             % Desplazamiento
%             PP = PP + C;
%             
%             % Arreglo para graficar
%             E = [PP(:, 1)', PP(:, 2)'; PP(:, 3)', PP(:, 4)'];
%             
%             % Gr�fica
%             img_interna = double(mascara);
%             img_interna = insertShape(img_interna, 'line', E, 'LineWidth', 4);
            
        end % clasificarImagen
        
        
    end % Methods private
    
    
end