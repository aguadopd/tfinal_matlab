classdef ClasificadorAltura < Clasificador
    %CLASIFICADORALTURA [ALMENDRAS] Lado mayor de cuadro envolvente rotado.
    %
    % Este clasificador analiza la altura del objeto, la cu�l se define
    % como el lado mayor del cuadro m�nimo que envuelve al objeto, una vez
    % que ha sido horizontalizado.
    %
    % La horizontalizaci�n se realiza seg�n el �ngulo de elevaci�n de una
    % elipse con los mismos segundos momentos que el objeto. Se supone que
    % el objeto es la mancha o _blob_ de mayor tama�o en la imagen de
    % m�scara.
    %
    % La altura se define como la media de las alturas encontradas en la
    % imagen de frente y la de dorso.
    %
    % Etiqueta seg�n par�metros variables. Si se encuentra fuera de
    % un cierto rango, se considera NoAlmendra. Podr�a ser Mala.
    %
    % PARAMETROS
    %   *umbral_min*: Altura m�nima para ser considerado almendra, en
    %   p�xeles.
    %   *umbral_max*: Altura m�xima para ser considerado almendra, en
    %   p�xeles.
    %
    % TODO: PASAR A MEDIDAS REALES. INCLUIR FACTOR DE CONVERSI�N.
    
    
    %% Propiedades p�blicas
    % Todos los par�metros que se puedan variar durante la ejecuci�n del
    % programa, probablemente a trav�s de la GUI.
    properties(SetAccess = public)
    end
    
    
    %% Propiedades privadas
    properties(SetAccess = private)
    end
    
    
    %% M�todos p�blicos
    methods(Access = public)
        
        
        function obj = ClasificadorAltura(nombre, varargin)
            % Constructor
            
            % Validaci�n de entradas
            % ToDo
            
            % Constructor de superclase
            obj@Clasificador(nombre);
            
            % Par�metros
            % Par�metros variables de esta etapa. Son miembros de la
            % estructura p. Su formato:
            % Parametro(por_defecto, min, max, valor);
            
            % *umbral_min*: Altura m�nima para ser considerado almendra. En
            % p�xeles.
            obj.p.umbral_min = Parametro(double(425), 0, 1000, 425);
            
            % *umbral_max*: Altura m�xima para ser considerado almendra. En
            % p�xeles.
            obj.p.umbral_max = Parametro(double(589), 0, 1000, 589);            
            
        end
        
        
    end
    
    
    %% M�todos protegidos
    methods(Access = protected)
        
        
        function [clase, internos] = clasificarResultados(obj, resultados)
            %CLASIFICARRESULTADOS Clasifica un Resultado
            % Uso: [clase, internos] = clasificarResultados(obj, resultados)
            % *clase*: Objeto de la clase Clases. Etiqueta.
            % *internos*: Estructura de informaci�n que emite la etapa
            %   clasificadora. Por ejemplo, valores que midi�, o im�genes �tiles
            %   para depuraci�n. En uso actualmente: campo 'valor', es la
            %   medici�n m�s importante que realiza la etapa. El descriptor.
            % *resultados*: Objeto de la clase Resultados
            
            % Estructura de resultados internos.
            internos = struct();
            
            % Clasificaci�n de frente y dorso
            [altura_frente, internos.imagen_frente] = ...
                obj.clasificarImagen(resultados.mascara_frente);
            [altura_dorso, internos.imagen_dorso] = ...
                obj.clasificarImagen(resultados.mascara_dorso);
            
            % La altura del objeto es la media de las alturas encontradas
            % en la imagen de frente y la de dorso.
            altura = mean([altura_frente, altura_dorso]);
            
            % Se guarda el valor en la estructura de resultados internos.
            internos.valor = altura;
            
            % Etiqueta seg�n par�metros variables. Si se encuentra fuera de
            % un cierto rango, se considera NoAlmendra. Podr�a ser Mala.
            if altura < obj.p.umbral_min.valor || altura > obj.p.umbral_max.valor
                clase = Clases.NoAlmendra;
            else
                clase = Clases.Primera;
            end
            
        end
        
        
    end % methods protected
    
    
    %% M�todos privados
    methods(Access = private)
        
        
        function [altura, img] = clasificarImagen(obj, mascara)
            %CLASIFICARIMAGEN Clasifica una imagen de m�scara
            % Uso: [altura, img] = clasificarImagen(obj, i_mascara)
            % *altura*: altura en p�xeles del objeto de mayor tama�o de la
            % imagen de m�scara, una vez horizontalizado.
            % *img*: Una im�gen de la m�scara rotada, con el cuadro
            % envolvente (bounding box) superpuesto. �til para depuraci�n.
            
            % Validaci�n de entradas
            % La mascara es bidimensional y l�gica
            validateattributes(mascara, {'logical'}, {'nonempty', 'ndims', 2}, ...
                'clasificarImagen', 'i_mascara', 1);
            
            
            % ROTACI�N
            % C�lculo de la orientaci�n de las regiones conectadas en la
            % m�scara.
            propiedades = regionprops(mascara, 'Orientation');
            
            % Suponemos que s�lo hay una regi�n conectada ---solamente 1
            % objeto. Por las dudas, usamos la primera.
            propiedades = propiedades(1);
            
            % Rotaci�n seg�n el �ngulo entre el eje horizontal y el eje
            % mayor de la elipse ajustada con los mismos momentos de
            % segundo orden que la regi�n. Rotamos para tener la menor
            % bounding box posible. Queda horizontal.
            tita = deg2rad(-propiedades.Orientation);
            mascara_rotada = imrotate(mascara, rad2deg(tita));
            
            
            % ALTURA
            % Rec�lculo de propiedades de las regiones conectadas.
            propiedades_rotada = regionprops(mascara_rotada, 'BoundingBox');
            
            altura = propiedades_rotada(1).BoundingBox(3);
            
            
            % Creaci�n de imagen auxiliar. Es la m�scara rotada con la caja
            % envolvente dibujada encima.
            % Ojo, es pesado. DESHABILITADO
            img = [];
            %             mascara_rotada = double(mascara_rotada);
            %             img = insertShape(mascara_rotada, 'rectangle', propiedades_rotada(1).BoundingBox, 'LineWidth', 3);
            
        end % clasificarImagen
        
        
    end % Methods private
    
    
end