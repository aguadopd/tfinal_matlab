classdef ClasificadorCIELabMedia < Clasificador
    %CLASIFICADORCIELABMEDIA [ALMENDRAS] Media del color seg�n CIELab
    %
    % Descriptor: Media de las componentes a* y b* seg�n el espacio de
    % color CIE L*a*b* 1976, con blanco de referencia D65. En caso de
    % ser almendras, puede forzarse el an�lisis a las zonas con tegumento
    % �nicamente. Esto �ltimo si la m�scara usada es la m�scara sin zonas
    % peladas.
    %
    % Los valores medidos son la media de los valores de frente y dorso.
    %
    % Este clasificador busca detectar almendras de color malo u objetos
    % que no sean almendras.
    %
    % Si los valores calculados para el frente o para el dorso se exceden
    % de cierto valor l�mite, se considera que es una almendra Mala.
    %
    % PARAMETROS
    %   *a_min*: Valor m�nimo de a*.
    %       -127 <= umbral_min < a_max
    %   *a_max*: Valor m�ximo de a*.
    %       a_min < a_max <= 128
    %   *b_min*: Valor m�nimo de b*.
    %       -127 <= umbral_min < b_max
    %   *b_max*: Valor m�ximo de b*.
    %       b_min < b_max <= 128
    %   *radio_erosion*: Radio en p�xeles de un disco con el que se
    %   erosiona la m�scara antes de calcular, ya que no hay buena
    %   informaci�n de color en los bordes.
    %       0 <= radio_erosion <= 50
    %
    % Nota: El rango [-127, 128] es arbitrario de mi parte. En realidad
    % depende de la implementaci�n de CIEL*a*b*, pero no encuentro nada en
    % la documentaci�n de Matlab.
    
    
    %% Propiedades p�blicas
    % Todos los par�metros que se puedan variar durante la ejecuci�n del
    % programa, probablemente a trav�s de la GUI.
    properties(SetAccess = public)
    end
    
    
    %% Propiedades privadas
    properties(SetAccess = private)
    end
    
    
    %% M�todos p�blicos
    methods(Access = public)
        
        
        function obj = ClasificadorCIELabMedia(nombre, varargin)
            % Constructor
            
            % Validaci�n de entradas
            % ToDo
            
            % Constructor de superclase
            obj@Clasificador(nombre);
            
            % Par�metros
            % Par�metros variables de esta etapa. Son miembros de la
            % estructura p. Su formato:
            % Parametro(por_defecto, min, max, valor);
            
            %   *a_min*: Valor m�nimo de a*.
            %       0 <= umbral_min < a_max
            obj.p.a_min = Parametro(double(6), -127, 128, 6);
            
            %   *a_max*: Valor m�ximo de a*.
            %       a_min < a_max <= 100
            obj.p.a_max = Parametro(double(20), -127, 128, 20);
            
            %   *b_min*: Valor m�nimo de b*.
            %       0 <= umbral_min < b_max
            obj.p.b_min = Parametro(double(6), -127, 128, 6);
            
            %   *b_max*: Valor m�ximo de b*.
            %       b_min < b_max <= 100
            obj.p.b_max = Parametro(double(32), -127, 128, 32);
            
            %   *radio_erosion*: Radio en p�xeles de un disco con el que se
            %   erosiona la m�scara antes de calcular, ya que no hay buena
            %   informaci�n de color en los bordes.
            %       0 <= radio_erosion <= 50
            obj.p.radio_erosion = Parametro(int8(10), 0, 50, 10);
            
        end
        
    end
    
    
    %% M�todos protegidos
    methods(Access = protected)
        
        
        function [clase, internos] = clasificarResultados(obj, resultados)
            %CLASIFICARRESULTADOS Clasifica un Resultado
            % Uso: [clase, internos] = clasificarResultados(obj, resultados)
            % *clase*: Objeto de la clase Clases. Etiqueta.
            % *internos*: Estructura de informaci�n que emite la etapa
            %   clasificadora. Por ejemplo, valores que midi�, o im�genes �tiles
            %   para depuraci�n. En uso actualmente: campo 'valor', es la
            %   medici�n m�s importante que realiza la etapa. El descriptor.
            % *resultados*: Objeto de la clase Resultados
            
            % Estructura de resultados internos.
            internos = struct();
            
            % C�lculos en frente y dorso. Se puede usar
            %   mascara_frente o mascara_frente_tegumento.
            
            % Intentamos usar m�scara de tegumento. Si est� vac�a, se usa la del objeto.
            if any(resultados.mascara_frente_tegumento)
                mascara_frente = resultados.mascara_frente_tegumento;
            else
                mascara_frente = resultados.mascara_frente;
            end
            
            if any(resultados.mascara_dorso_tegumento)
                mascara_dorso = resultados.mascara_dorso_tegumento;
            else
                mascara_dorso = resultados.mascara_dorso;
            end
            
            [media_a_frente, media_b_frente] = ...
                obj.clasificarImagen(resultados.imagen_frente, mascara_frente);
            [media_a_dorso, media_b_dorso] = ...
                obj.clasificarImagen(resultados.imagen_dorso, mascara_dorso);
            
            
            % Las m�tricas son los promedios de los valores de frente y
            % dorso.
            medias_a = mean([media_a_frente, media_a_dorso]);
            medias_b = mean([media_b_frente, media_b_dorso]);
            
            % Se guardan las medidas.
            internos.valor = [medias_a, medias_b];
            
            % Clasificaci�n
            if medias_a < obj.p.a_min.valor || medias_a > obj.p.a_max.valor
                clase = Clases.Mala;
            elseif medias_b < obj.p.b_min.valor || medias_b > obj.p.b_max.valor
                clase = Clases.Mala;
            else
                clase = Clases.Primera;
            end
            
        end
        
        
    end % methods protected
    
    
    %% M�todos privados
    methods(Access = private)
        
        
        function [media_a, media_b] = clasificarImagen(obj, imagen, mascara)
            %CLASIFICARIMAGEN
            % Uso: [desviacion] = clasificarImagen(obj, imagen, mascara)
            % *media_a*: Media de la componente a* en el objeto.
            % *media_b*: Media de la componente b* en el objeto.
            % *imagen*: Imagen RGB.
            % *mascara*: Imagen binaria. Fondo negro, objeto en blanco.
            %
            % Seg�n CIE L*a*b* 1976, con blanco de referencia D65.
            
            
            % Validaci�n de entradas
            % ToDo
            %- La mascara es bidimensional y l�gica
            validateattributes(mascara, {'logical'}, {'nonempty', 'ndims', 2}, ...
                'clasificarImagen', 'i_mascara', 2);
            
            
            % Erosi�n de la m�scara, porque en los bordes no hay
            % informaci�n de color confiable.
            SE = strel('disk', obj.p.radio_erosion.valor);
            mascara = imerode(mascara, SE);
            
            % Conversi�n a CIEL*a*b*
            lab = rgb2lab(imagen);
            a = lab(:,:, 2);
            b = lab(:,:, 3);
            
            % Aplicaci�n de m�scara
            a = a(mascara);
            b = b(mascara);
            
            
            % C�LCULOS
            media_a = mean(a);
            media_b = mean(b);
            
        end % clasificarImagen
        
        
    end % Methods private
    
    
end