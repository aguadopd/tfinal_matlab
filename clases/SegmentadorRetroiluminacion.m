classdef SegmentadorRetroiluminacion < Segmentador
    %SEGMENTADORRETROILUMINACION [ALMENDRAS]
    % Genera m�scaras binarias a partir de im�genes de objetos
    % retroiluminados, donde el contraste entre el fondo y el objeto es
    % grande.
    %
    % La imagen retroiluminada RGB se difumina con un filtro gaussiano, se
    % umbraliza seg�n Otsu y luego se erosiona. Por �ltimo, se selecciona
    % el objeto m�s grande y se rellenan sus agujeros, si los hay.
    %
    % Carga estas im�genes desde las direcciones que se encuentran en el
    % arreglo (cell) de datos de un objeto PrincipalSet. Este arreglo debe
    % pasarse como entrada.
    %
    % PARAMETROS
    % *sigma_blur*: Desviaci�n est�ndar de la distribuci�n gaussiana
    %  del kernel de filtrado, en p�xeles.
    %       0 <= sigma < 50
    % *radio_erosion*: Radio de un disco que erosiona la imagen
    % umbralizada, en p�xeles.
    %       0 <= radio_erosion < 50
    
    
    %% Propiedades p�blicas
    % Todos los par�metros que se puedan variar durante la ejecuci�n del
    % programa, probablemente a trav�s de la GUI.
    properties(SetAccess = public)
        % Arreglo tipo cell que contiene informaci�n sobre las im�genes que
        % se est�n procesando. Mismo formato que la propiedad 'datos' de la
        % clase PrincipalSet.
        cellarchivos
    end
    
    
    %% M�todos p�blicos
    methods
        
        
        function obj = SegmentadorRetroiluminacion(nombre, cellarchivos, varargin)
            % Constructor
            % Ej: PS.agregar( SegmentadorRetroiluminacion('retro', PS.datos) );
            
            % Validaci�n de entradas
            % ToDo
            
            % Constructor de superclase
            obj@Segmentador(nombre);
            
            % Arreglo tipo cell que contiene informaci�n sobre las im�genes que
            % se est�n procesando. Mismo formato que la propiedad 'datos' de la
            % clase PrincipalSet.
            obj.cellarchivos = cellarchivos;
            
            
            % Par�metros
            % Par�metros variables de esta etapa. Son miembros de la
            % estructura p. Su formato:
            % Parametro(por_defecto, min, max, valor);
            
            % *radio_erosion*: Radio de un disco que erosiona la imagen
            % umbralizada, en p�xeles.
            %       0 <= radio_erosion < 50
            obj.p.radio_erosion = Parametro(int8(1), 1, 50, 1);
            
            % *sigma_blur*: Desviaci�n est�ndar de la distribuci�n gaussiana
            %  del kernel de filtrado, en p�xeles.
            %       0 <= sigma < 50
            obj.p.sigma_blur = Parametro(double(2), 0, 50, 2);
            
        end
        
        
    end
    
    
    %% M�todos protegidos
    methods(Access = protected)
        
        
        function [mascara_frente, mascara_dorso, hay_objeto, internos] = segmentarResultados(obj, resultados)
            %SEGMENTARRESULTADOS Segmenta un objeto de clase resultados
            % Uso: [mascara_frente, mascara_dorso, hay_objeto, internos] = ...
            %                          ||||segmentarResultados(obj, resultados)
            % *mascara_frente*: imagen de tipo Logical. En blanco (true), los
            % p�xeles que corresponden al objeto.
            % *mascara_dorso*: imagen de tipo Logical. En blanco (true), los
            % p�xeles que corresponden al objeto.
            % *hay_objeto*: Booleano que indica si se encontr� algo. No est� en
            % uso.
            % *internos*: Estructura de informaci�n que emite la etapa.
            %   Por ejemplo, valores que midi�, o im�genes �tiles
            %   para depuraci�n.
            % *resultados*: Objeto de la clase Resultados.
            
            % Validaci�n de entradas
            % ToDo
            
            % Lectura de im�genes de objetos retroiluminados.
            retro_frente = imread(obj.cellarchivos{resultados.id_numero, 2});
            retro_dorso = imread(obj.cellarchivos{resultados.id_numero, 4});
            
            % Segmentaci�n.
            mascara_frente = segmentarImagen(obj, retro_frente);
            mascara_dorso = segmentarImagen(obj, retro_dorso);
            
            % No se verifica si hay objeto.
            hay_objeto = true;
            
            % Estructura de resultados internos. Vac�a.
            internos = struct();
            
        end
        
        
    end % methods protected
    
    
    %% M�todos privados
    methods(Access = private)
        
        
        function mascara = segmentarImagen(obj, imagen)
            %SEGMENTARIMAGEN Crea una m�scara
            % Uso: mascara = segmentarImagen(obj, imagen)
            % *mascara*: Arreglo bidimensional de tipo logical. En blanco
            % (true), los p�xeles que corresponden al objeto.
            % *imagen*: Imagen RGB uint8.
            %
            % La imagen retroiluminada RGB se difumina con un filtro
            % gaussiano, se umbraliza seg�n Otsu y luego se erosiona. Por
            % �ltimo, se selecciona el objeto m�s grande y se rellenan sus
            % agujeros, si los hay.
            %
            % ToDo: Contemplar que no haya quedado nada, o que la imagen
            % est� vac�a.
            
            % Validaci�n de entradas.
            % La imagen es a color, 3 canales: RGB
            imagen = imagen;
            validateattributes(imagen, {'uint8'}, {'nonempty', 'ndims', 3, 'size', [NaN, NaN, 3]}, ...
                'segmentar', 'imagen', 1);
            
            % Conversi�n a gris
            g = rgb2gray(imagen);
            
            % Blur gaussiano
            filtrada = imgaussfilt(g, obj.p.sigma_blur.valor);
            
            % Umbralizaci�n seg�n Otsu
            umbral = graythresh(filtrada);
            m = im2bw(filtrada, umbral);
            
            % Inversi�n. Lo blanco ser� el objeto.
            m = ~m;
            
            % Erosi�n de la m�scara, porque en general queda un poco
            % grande.
            SE = strel('disk', obj.p.radio_erosion.valor);
            m = imerode(m, SE);
            
            
            % Segmentaci�n de componentes conectados en la imagen binaria.
            % ToDo: Contemplar que no haya quedado nada, o que la imagen
            % est� vac�a.
            CC = bwconncomp(m);
            L = labelmatrix(CC);
            
            % Solo queda el objeto m�s grande. Se supone que el resto son errores.
            numPixels = cellfun(@numel, CC.PixelIdxList);
            [~, idx] = max(numPixels);
            mascara = (L == idx);
            
            % Relleno
            mascara = imfill(mascara, 'holes');
            
        end
        
        
    end % Methods private
    
    
end % classdef