classdef SegmentadorBorde1 < Segmentador
    %SEGMENTADORBORDE1 Segmentador por detecci�n de borde
    % Crea m�scaras binarias a partir del borde del objeto. La imagen a
    % color se convierte a escala de grises y luego se detectan bordes
    % seg�n Sobel. Prosigue un filtrado morfol�gico para conectar bordes y
    % eliminar detalles peque�os. A continuaci�n se selecciona el objeto
    % m�s grande (seg�n componentes conectados), se rellena y por �ltimo se
    % suaviza con m�s operaciones morfol�gicas.
    %
    % PAR�METROS
    % *cierre_radio*: radio de un disco que cierra la imagen umbralizada.
    %       0 <= cierre_radio < 50
    % *cierre_largo*: largo de una l�nea que cierra la imagen umbralizada,
    % a 0�, 45�, -45� y 90�.
    %       0 <= cierre_largo < 50
    % *apertura_radio*: radio de un disco que abre la m�scara final.
    %       0 <= apertura_radio < 50
    % *apertura_largo*: largo de una l�nea que abre la m�scara final,
    % a 0�, 45�, -45� y 90�.
    %       0 <= apertura_largo < 50
		% *radio_erosion*: Radio de un disco que erosiona la m�scara
		% final, en p�xeles.
		%       0 <= radio_erosion < 50
    % *umbral*: Umbral para la detecci�n de borde por Sobel.
    %       0 <= umbral <= 1
    
    
    %% Propiedades p�blicas
    % Todos los par�metros que se puedan variar durante la ejecuci�n del
    % programa, probablemente a trav�s de la GUI.
    properties(SetAccess = public)
    end
    
    %% M�todos p�blicos
    methods
        
        
        function obj = SegmentadorBorde1(nombre, varargin)
            % Constructor
            
            % Validaci�n de entradas
            % ToDo
            
            % Constructor de superclase
            obj@Segmentador(nombre);
            
            
            % Par�metros
            % Par�metros variables de esta etapa. Son miembros de la
            % estructura p. Su formato:
            % Parametro(por_defecto, min, max, valor);
            
            % *cierre_radio*: radio de un disco que cierra la imagen umbralizada.
            %       0 <= cierre_radio < 50
            obj.p.cierre_radio = Parametro(double(1), 1, 50, 1);
            
            % *cierre_largo*: largo de una l�nea que cierra la imagen umbralizada,
            % a 0�, 45�, -45� y 90�.
            %       0 <= cierre_largo < 50
            obj.p.cierre_largo = Parametro(double(1), 1, 50, 1);
            
            % *apertura_radio*: radio de un disco que abre la m�scara final.
            %       0 <= apertura_radio < 50
            obj.p.apertura_radio = Parametro(double(5), 1, 50, 5);
            
            % *apertura_largo*: largo de una l�nea que abre la m�scara final,
            % a 0�, 45�, -45� y 90�.
            %       0 <= apertura_largo < 50
            obj.p.apertura_largo = Parametro(double(5), 1, 50, 5);
						
            % *radio_erosion*: Radio de un disco que erosiona la m�scara
            % final, en p�xeles.
            %       0 <= radio_erosion < 50
            obj.p.radio_erosion = Parametro(int8(1), 1, 50, 1);
            
            % *umbral*: Umbral para la detecci�n de borde por Sobel.
            %       0 <= umbral <= 1
            obj.p.umbral = Parametro(double(0.0055), 0, 1, 0.0055);
            
        end
        
        
    end
    
    
    %% M�todos protegidos
    methods(Access = protected)
        
        
        function [mascara_frente, mascara_dorso, hay_objeto, internos] = ...
                segmentarResultados(obj, resultados)
            %SEGMENTARRESULTADOS Segmenta un objeto de clase resultados
            % Uso: [mascara_frente, mascara_dorso, hay_objeto, internos] = ...
            %                               segmentarResultados(obj, resultados)
            % *mascara_frente*: imagen de tipo Logical. En blanco (true), los
            % p�xeles que corresponden al objeto.
            % *mascara_dorso*: imagen de tipo Logical. En blanco (true), los
            % p�xeles que corresponden al objeto.
            % *hay_objeto*: Booleano que indica si se encontr� algo. No est� en
            % uso.
            % *internos*: Estructura de informaci�n que emite la etapa.
            %   Por ejemplo, valores que midi�, o im�genes �tiles
            %   para depuraci�n.
            % *resultados*: Objeto de la clase Resultados.
            
            % Validaci�n de entradas
            % ToDo
            
            % Segmentaci�n
            mascara_frente = obj.segmentarImagen(resultados.imagen_frente);
            mascara_dorso = obj.segmentarImagen(resultados.imagen_dorso);
            
            
            % No se verifica si hay objeto.
            hay_objeto = true;
            
            % Estructura de resultados internos. Vac�a.
            internos = struct();
            
        end
        
        
    end % methods protected
    
    
    %% M�todos privados
    methods(Access = private)
        
        
        function mascara = segmentarImagen(obj, imagen)
            %SEGMENTARIMAGEN Crea una m�scara
            % Uso: mascara = segmentarImagen(obj, imagen)
            % *mascara*: Arreglo bidimensional de tipo logical. En blanco
            % (true), los p�xeles que corresponden al objeto.
            % *imagen*: Imagen RGB uint8.
            %
            % Crea m�scaras binarias a partir del borde del objeto. La
            % imagen a color se convierte a escala de grises y luego se
            % detectan bordes seg�n Sobel. Prosigue un filtrado morfol�gico
            % para conectar bordes y eliminar detalles peque�os. A
            % continuaci�n se selecciona el objeto m�s grande (seg�n
            % componentes conectados), se rellena y por �ltimo se suaviza
            % con m�s operaciones morfol�gicas.
            %
            % ToDo: Contemplar que no haya quedado nada, o que la imagen
            % est� vac�a.
            
            % Validaci�n de entradas
            % La imagen es a color, 3 canales: RGB
            validateattributes(imagen, {'uint8'}, {'nonempty', 'ndims', 3, ...
                'size', [NaN, NaN, 3]}, 'segmentar', 'imagen', 1);
            
            % Conversi�n a gris
            g = rgb2gray(imagen);
            
            % Detecci�n de bordes
            [bsobel, threshold_sobel] = edge(g, 'Sobel', 'nothinning', obj.p.umbral.valor);
            
            % Filtrado morfol�gico para conectar bordes y filtrar cosas
            % peque�as.
            % Se utiliza un disco y una l�nea a distintos �ngulos.
            SEc = strel('disk', obj.p.cierre_radio.valor);
            bsobel = imclose(bsobel, SEc);
            
            SEc = strel('line', obj.p.cierre_largo.valor, 0);
            bsobel = imclose(bsobel, SEc);
            
            SEc = strel('line', obj.p.cierre_largo.valor, 90);
            bsobel = imclose(bsobel, SEc);
            
            SEc = strel('line', obj.p.cierre_largo.valor, 45);
            bsobel = imclose(bsobel, SEc);
            
            SEc = strel('line', obj.p.cierre_largo.valor, -45);
            bsobel = imclose(bsobel, SEc);
            
            SEc = strel('disk', obj.p.cierre_radio.valor);
            bsobel = imclose(bsobel, SEc);
            
            
            % Segmentaci�n de componentes conectados en la imagen binaria
            CC = bwconncomp(bsobel);
            L = labelmatrix(CC);
            
            % Solo queda el objeto m�s grande. Se supone que el resto son errores.
            numPixels = cellfun(@numel, CC.PixelIdxList);
            [~, idx] = max(numPixels);
            mascara = (L == idx);
            
            % Relleno
            mascara = imfill(mascara, 'holes');
            
            
            % Suavizado morfol�gico
            % Apertura con disco y l�neas a distintos �ngulos para
            % contemplar diversos defectos.
            SEo = strel('disk', obj.p.apertura_radio.valor);
            mascara = imopen(mascara, SEo);
            
            SEo = strel('line', obj.p.apertura_largo.valor, 0);
            mascara = imopen(mascara, SEo);
            
            SEo = strel('line', obj.p.apertura_largo.valor, 90);
            mascara = imopen(mascara, SEo);
            
            SEo = strel('line', obj.p.apertura_largo.valor, 45);
            mascara = imopen(mascara, SEo);
            
            SEo = strel('line', obj.p.apertura_largo.valor, -45);
            mascara = imopen(mascara, SEo);
						
						% Erosi�n de la m�scara, por si queda grande
            SEe = strel('disk', obj.p.radio_erosion.valor);
            mascara = imerode(mascara, SEe);
            
        end
        
        
    end % Methods private
    
    
end % classdef