classdef SegmentadorHue1 < Segmentador
    %SEGMENTADORHUE1 [ALMENDRAS] Segmentador por matiz
    % Crea m�scaras binarias a partir de una umbralizaci�n del canal H.
    % Sirve si hay una diferencia importante entre los tonos del fondo y de
    % los objetos. Para almendras, un color azul es ideal porque es el
    % complementario, opuesto.
    %
    % El canal H se umbraliza seg�n Otsu. Luego se filtra morfol�gicamente,
    % con un cierre y una apertura. Por �ltimo, se selecciona el objeto m�s
    % grande.
    %
    % PARAMETROS
    % *cierre_radio*: radio de un disco que cierra la imagen umbralizada.
    %       0 <= cierre_radio < 50
    % *apertura_radio*: radio de un disco que abre la imagen umbralizada.
    %       0 <= apertura_radio < 50
    
    
    %% Propiedades p�blicas
    % Todos los par�metros que se puedan variar durante la ejecuci�n del
    % programa, probablemente a trav�s de la GUI.
    properties(SetAccess = public)
    end
    
    
    %% M�todos privados
    methods
        
        
        function obj = SegmentadorHue1(nombre, varargin)
            % Constructor
            
            % Validaci�n de entradas
            % ToDo
            
            % Constructor de superclase
            obj@Segmentador(nombre);
            
            
            % Par�metros
            % Par�metros variables de esta etapa. Son miembros de la
            % estructura p. Su formato:
            % Parametro(por_defecto, min, max, valor);
            
            % *cierre_radio*: radio de un disco que cierra la imagen umbralizada.
            %       0 <= cierre_radio < 50
            obj.p.cierre_radio = Parametro(4, 1, 50, 4);
            
            % *apertura_radio*: radio de un disco que abre la imagen umbralizada.
            %       0 <= apertura_radio < 50
            obj.p.apertura_radio = Parametro(2, 1, 50, 2);
            
        end
        
        
    end
    
    
    %% M�todos protegidos
    methods(Access = protected)
        
        
        function [mascara_frente, mascara_dorso, hay_objeto, internos] = segmentarResultados(obj, resultados)
            %SEGMENTARRESULTADOS Segmenta un objeto de clase resultados
            % Uso: [mascara_frente, mascara_dorso, hay_objeto, internos] = ...
            %                          ||||segmentarResultados(obj, resultados)
            % *mascara_frente*: imagen de tipo Logical. En blanco (true), los
            % p�xeles que corresponden al objeto.
            % *mascara_dorso*: imagen de tipo Logical. En blanco (true), los
            % p�xeles que corresponden al objeto.
            % *hay_objeto*: Booleano que indica si se encontr� algo. No est� en
            % uso.
            % *internos*: Estructura de informaci�n que emite la etapa.
            %   Por ejemplo, valores que midi�, o im�genes �tiles
            %   para depuraci�n.
            % *resultados*: Objeto de la clase Resultados.
            
            % Validaci�n de entradas
            % ToDo
            
            % Segmentaci�n
            mascara_frente = obj.segmentarImagen(resultados.imagen_frente);
            mascara_dorso = obj.segmentarImagen(resultados.imagen_dorso);
            
            
            % No se verifica si hay objeto.
            hay_objeto = true;
            
            % Estructura de resultados internos. Vac�a.
            internos = struct();
        end
        
        
    end % methods protected
    
    
    %% M�todos privados
    methods(Access = private)
        
        
        function mascara = segmentarImagen(obj, imagen)
            %SEGMENTARIMAGEN Crea una m�scara
            % Uso: mascara = segmentarImagen(obj, imagen)
            % *mascara*: Arreglo bidimensional de tipo logical. En blanco
            % (true), los p�xeles que corresponden al objeto.
            % *imagen*: Imagen RGB uint8.
            %
            % Crea m�scaras binarias a partir de una umbralizaci�n del
            % canal H. Sirve si hay una diferencia importante entre los
            % tonos del fondo y de los objetos. Para almendras, un color
            % azul es ideal porque es el complementario, opuesto.
            %
            % El canal H se umbraliza seg�n Otsu. Luego se filtra
            % morfol�gicamente, con un cierre y una apertura. Por �ltimo,
            % se selecciona el objeto m�s grande.
            %
            % ToDo: Contemplar que no haya quedado nada, o que la imagen
            % est� vac�a.
            
            % Validaci�n de entradas
            %- La imagen es a color, 3 canales: RGB
            validateattributes(imagen, {'uint8'}, {'nonempty', 'ndims', 3, 'size', [NaN, NaN, 3]}, ...
                'segmentar', 'imagen', 1);
            
            % SEGMENTACION
            % Conversi�n a HSV
            hsv = rgb2hsv(imagen);
            h = hsv(:,:, 1);
            % s = hsv(:,:,2);
            % v = hsv(:,:,3)
            
            % Determinaci�n del umbral en el histograma, m�todo de Otsu
            umbral = graythresh(h);
            % Binarizaci�n
            h_umbralizada = im2bw(h, umbral);
            
            % Inversa condicional. Se supone que hay m�s fondo que objeto.
            % ToDo: esto deber�a hacerse seg�n el color del fondo. No
            % necesariamiente hay m�s fondo que objeto.
            suma_inferior = sum(sum(h < umbral));
            suma_superior = numel(h) - suma_inferior;
            if suma_inferior < suma_superior
                h_umbralizada = ~h_umbralizada;
            end
            
            
            % FILTRADO
            % Cierre
            SEc = strel('disk', obj.p.cierre_radio.valor);
            h_umbralizada = imclose(h_umbralizada, SEc);
            
            % Apertura
            SEa = strel('disk', obj.p.apertura_radio.valor);
            h_umbralizada = imopen(h_umbralizada, SEa);
            
            % Segmentaci�n de componentes conectados en la imagen binaria
            CC = bwconncomp(h_umbralizada);
            L = labelmatrix(CC);
            
            % Solo queda el objeto m�s grande. Se supone que el resto son errores.
            numPixels = cellfun(@numel, CC.PixelIdxList);
            [~, idx] = max(numPixels);
            mascara = (L == idx);
						
						% Relleno
            mascara = imfill(mascara, 'holes');
            
        end
        
        
    end % Methods private
    
    
end % classdef