classdef Resultados < matlab.mixin.Copyable
    %RESULTADOS [ALMENDRAS] Imagen, m�scara y clasificaci�n
    % Esta clase contiene im�genes del objeto a analizar, m�scaras e
    % informaci�n sobre su clasificaci�n.
    % En este momento est� funcionando para solo un objeto por imagen. Para
    % contemplar muchos objetos por imagen, un segmentador grueso deber�a
    % identificar m�ltiples objetos en una imagen y crear tantos objetos
    % Resultados como objetos haya en la imagen.
    
    
    %% Propiedades p�blicas
    %   De modificaci�n p�blica
    properties(GetAccess = public, SetAccess = public)
        % Im�genes que se modifican. En general, son las que usan las
        % etapas clasificadoras y segmentadoras.
        imagen_frente
        imagen_dorso
        
        % M�scara general que determina la zona del objeto, si lo hay.
        mascara_frente = [];
        mascara_dorso = [];
        
        % M�scara s�lo de tegumento/piel.
        mascara_frente_tegumento = [];
        mascara_dorso_tegumento = [];
        
        % Clase asignada.
        clase = Clases.Nada;
        
        % Cell de structs, donde cada struct tiene informaci�n extra
        % a�adida por cada etapa del algoritmo.
        internos = cell(0);
        
        % Log de decisiones, en forma de cell de strings
        log = cell(0);
        
        % Causa de rechazo. String
        causa_rechazo = char(0);
    end
    
    %   De modificaci�n privada. Sus valores solo se modificar�n al crear
    %   el objeto.
    properties(GetAccess = public, SetAccess = private)
        % Im�genes originales. Para cualquier etapa que las necesite.
        % Particularmente, los preprocesadores iniciales.
        imagen_original_frente
        imagen_original_dorso
        
        % N�mero de identificaci�n. �til para trabajar con sets de
        % im�genes.
        id_numero
    end
    
    
    %% Propiedades privadas
    properties(GetAccess = private, SetAccess = private)
    end
    
    
    %% M�todos p�blicos
    methods(Access = public)
        
        
        function obj = Resultados(imagen_frente, imagen_dorso, id_numero)
            % Constructor
            
            % Validaci�n de entradas
            % ToDO
            validateattributes(imagen_frente, {'uint8'}, {'nonempty', 'ndims', 3, 'size', [NaN, NaN, 3]}, ...
                'Resultados', 'i_img_f', 1);
            validateattributes(imagen_dorso, {'uint8'}, {'nonempty', 'ndims', 3, 'size', [NaN, NaN, 3]}, ...
                'Resultados', 'i_img_r', 2);
            
            % Inicializaci�n
            obj.imagen_original_frente = imagen_frente;
            obj.imagen_frente = imagen_frente;
            obj.imagen_original_dorso = imagen_dorso;
            obj.imagen_dorso = imagen_dorso;
            obj.id_numero = id_numero;
            
        end
        
        
    end
    
    
    %% M�todos privados
    methods(Access = private)
    end
    
    
end