classdef Segmentador < matlab.mixin.Copyable
    %SEGMENTADOR [ALMENDRAS] Superclase para los segmentadores
    % Los segmentadores buscan los objetos que est�n en las im�genes y
    % crean una m�scara binaria que identifica sus l�mites. Generalmente
    % las etapas de segmentaci�n se ejecutaran luego del preprocesamiento
    % y antes de la clasificaci�n.
    %
    % **SUPONE QUE SOLO HAY UN OBJETO POR RESULTADO**
    %
    % Cada objeto de clase Resultados tiene las siguientes propiedades
    % �tiles y p�blicas:
    %   M�scara general que determina la zona del objeto, si lo hay
    %       mascara_frente = [];
    %       mascara_dorso = [];
    %   M�scara s�lo de tegumento/piel.
    %       mascara_frente_tegumento = [];
    %       mascara_dorso_tegumento = [];
    %
    % Los segmentadores crean 'mascara_frente' y 'mascara_dorso'.
    
    
    %% Propiedades p�blicas
    % Todos los par�metros que se puedan variar durante la ejecuci�n del
    % programa, probablemente a trav�s de la GUI.
    properties(SetAccess = public)
        % Par�metros de cada clasificador. 'p' para brevedad. Es una
        % estructura de objetos de la clase Parametro.
        p = struct();
    end
    
    
    %% Propiedades privadas
    properties(SetAccess = protected)
        % Nombre del objeto creado.
        nombre_objeto = '';
    end
    
    
    %% M�todos p�blicos
    methods
        
        
        function obj = Segmentador(nombre, varargin)
            % Constructor
            
            % Validaci�n de entradas
            % ToDo
            
            % Nombre para f�cil identificaci�n.
            obj.nombre_objeto = nombre;
        end
        
        
        % Segmentar. Rellena objeto Resultados
        function resultados = segmentar(obj, resultados)
            %SEGMENTAR Rellena objeto Resultados
            % Uso: resultados = segmentar(obj, resultados)
            % *resultados* = Objeto de la clase Resultados
            %
            % Modifica las propiedades p�blicas 'mascara_frente' e
            % 'mascara_dorso' del objeto 'resultados'.
            
            % Validaci�n de entradas
            % ToDo
            
            
            % Segmentaci�n
            [mascara_frente, mascara_dorso, hay_objeto, internos] = segmentarResultados(obj, resultados);
            resultados.mascara_frente = mascara_frente;
            resultados.mascara_dorso = mascara_dorso;
            
            % Si hay un objeto, suponemos que se trata de una almendra
            % buena.
            % _Actualmente no se usa_.
            if hay_objeto == true
                resultados.clase = Clases.Primera;
            end
            
            % Se guarda la estructura de metainformaci�n de esta etapa.
            internos.nombre_objeto = obj.nombre_objeto;
            resultados.internos{end+1} = internos;
            
        end % segmentar
        
        
    end % methods
    
    
    %% M�todos abstractos
    methods(Abstract = true, Access = protected)
        
        
        %SEGMENTARRESULTADOS Segmenta un objeto de clase resultados
        % Uso: [mascara_frente, mascara_dorso, hay_objeto, internos] = ...
        %                                                       segmentarResultados(obj, resultados)
        % *mascara_frente*: imagen de tipo Logical. En blanco (true), los
        % p�xeles que corresponden al objeto.
        % *mascara_dorso*: imagen de tipo Logical. En blanco (true), los
        % p�xeles que corresponden al objeto.
        % *hay_objeto*: Booleano que indica si se encontr� algo. No est� en
        % uso.
        % *internos*: Estructura de informaci�n que emite la etapa.
        %   Por ejemplo, valores que midi�, o im�genes �tiles
        %   para depuraci�n.
        % *resultados*: Objeto de la clase Resultados.
        [mascara_frente, mascara_dorso, hay_objeto, internos] = segmentarResultados(obj, resultados)
        
        
    end % methods ( Abstract = true )
    
    
end % classdef