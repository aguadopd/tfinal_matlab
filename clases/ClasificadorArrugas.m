classdef ClasificadorArrugas < Clasificador
    %CLASIFICADORARRUGAS [ALMENDRAS] �rea arrugada / �rea m�scara
    %
    % Se estima �rea_arrugada/�rea_total_m�scara como el valor de
    % �rea_oscura/�rea_total_m�scara.
    %
    % Este clasificador intenta distinguir las almendras muy arrugadas.
    % Para ello primero convierte la imagen a HSV. Umbraliza el canal V en
    % un valor bajo, como para quedarse con las �reas m�s sombreadas y que
    % pueden indicar presencia de arrugas.
    %
    % La m�trica final para cada lado es �rea_con_sombras / �rea_total.
    % Esta es calculada como 1 - area_sin_sombras / �rea_total.
    % Para el objeto, la m�trica es la media de lo medido en cada lado.
    % Esto permite que no se etiqueten err�neamente como malas algunas
    % almendras que tienen puntos grandes oscuros cerca de su base. En 
    % general las almendras arrugadas lo est�n en ambos lados.
    %
    % Etiqueta seg�n par�metros variables. Si la proporci�n arrugada
    % (oscura) supera un cierto valor, se considera Mala.
    %
    % PARAMETROS
    %   *v_min*: Valor de V que delimita zonas oscuras de claras. 0 <= v <= 1
    %   *umbral_max*: Proporci�n m�xima para ser considerada almendra buena.
    %       0 <= umbral_max <= 1
    
    
    %% Propiedades p�blicas
    % Todos los par�metros que se puedan variar durante la ejecuci�n del
    % programa, probablemente a trav�s de la GUI.
    properties(SetAccess = public)
    end
    
    
    %% Propiedades privadas
    properties(SetAccess = private)
    end
    
    
    %% M�todos p�blicos
    methods(Access = public)
        
        
        function obj = ClasificadorArrugas(nombre, varargin)
            % Constructor
            
            % Validaci�n de entradas
            % ToDo
            
            % Constructor de superclase
            obj@Clasificador(nombre);
            
            % Par�metros
            % Par�metros variables de esta etapa. Son miembros de la
            % estructura p. Su formato:
            % Parametro(por_defecto, min, max, valor);
            
            % *v_min*: Valor de V que delimita zonas oscuras de claras. 0 <= v <= 1
            obj.p.v_min = Parametro(double(0.12), 0, 1, 0.12);
            
            %   *umbral_max*: Proporci�n m�xima para ser considerada almendra buena.
            %       0 <= umbral_max <= 1
            obj.p.umbral_max = Parametro(double(0), 0, 1, 0.1);
            
        end
        
    end
    
    
    %% M�todos protegidos
    methods(Access = protected)
        
        
        function [clase, internos] = clasificarResultados(obj, resultados)
            %CLASIFICARRESULTADOS Clasifica un Resultado
            % Uso: [clase, internos] = clasificarResultados(obj, resultados)
            % *clase*: Objeto de la clase Clases. Etiqueta.
            % *internos*: Estructura de informaci�n que emite la etapa
            %   clasificadora. Por ejemplo, valores que midi�, o im�genes �tiles
            %   para depuraci�n. En uso actualmente: campo 'valor', es la
            %   medici�n m�s importante que realiza la etapa. El descriptor.
            % *resultados*: Objeto de la clase Resultados
            
            % Estructura de resultados internos.
            internos = struct();
            
            % Clasificaci�n de frente y dorso.
            [proporcion_frente, internos.imagen_frente] = ...
                obj.clasificarImagen(resultados.imagen_frente, resultados.mascara_frente);
            [proporcion_dorso, internos.imagen_dorso] = ...
                obj.clasificarImagen(resultados.imagen_dorso, resultados.mascara_dorso);
            
            % Se analiza la media: en general las almendras arrugadas est�n
            % arrugadas en ambos lados. Esto ayuda a salvar de la mala
            % clasificaci�n de algunas almendras que tienen zonas m�s
            % oscuras en la base.
            proporcion = mean([proporcion_frente, proporcion_dorso]);
            
            % Se guarda el valor en la estructura de resultados internos.
            internos.valor = proporcion;
            
            % Etiqueta seg�n par�metro variable.
            if proporcion > obj.p.umbral_max.valor
                clase = Clases.Mala;
            else
                clase = Clases.Primera;
                % Ac� se podr�a implementar Clases.Segunda
            end
            
        end
        
        
    end % methods protected
    
    
    %% M�todos privados
    methods(Access = private)
        
        
        function [proporcion, img_interna] = clasificarImagen(obj, imagen, mascara)
            %CLASIFICARIMAGEN Clasifica una imagen de m�scara
            % Uso: [proporcion, img_interna] = clasificarImagen(obj, imagen, mascara)
            % *proporcion*: �rea_arrugada / �rea_total_m�scara
            % *img_interna*: Una im�gen de la m�scara de la zona no oscura,
            % supuestamente la zona que no es arruga. �til para depuraci�n.
            
            % Validaci�n de entradas
            %- La mascara es bidimensional y l�gica
            validateattributes(mascara, {'logical'}, {'nonempty', 'ndims', 2}, ...
                'clasificarImagen', 'i_mascara', 2);
            % ToDO RGB
            
            
            % Conversi�n al espacio HSV
            hsv = rgb2hsv(imagen);
            canal_v = hsv(:,:, 3);
            
            % Aplicaci�n de m�scara
            canal_v = canal_v .* mascara;
            
            % Umbralizaci�n
            % Quedan en la m�scara solo aquellos p�xeles que en la imagen
            % original tengan un Valor mayor a v_min
            mascara_no_oscura = mascara;
            mascara_no_oscura(canal_v < obj.p.v_min.valor) = 0;
            
            
            % Mediciones
            area_no_oscura = sum(sum(mascara_no_oscura));
            area_mascara = sum(sum(mascara));
            proporcion = 1 - area_no_oscura / area_mascara;
            
            % Imagen auxiliar �til para depuraci�n.
            img_interna = double(mascara_no_oscura);
            
        end % clasificarImagen
        
        
    end % Methods private
    
    
end