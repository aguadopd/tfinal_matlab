classdef PreprocesadorBlur < Preprocesador
    %PREPROCESADORBLUR [Almendras] Difuminado gaussiano
    % Filtra las im�genes con un kernel de suavizado gaussiano.
    %
    % PARAMETROS
    %   *sigma*: Desviaci�n est�ndar de la distribuci�n gaussiana
    %   del kernel de filtrado, en p�xeles.
    %       0 <= sigma < inf
    
    
    %% Propiedades p�blicas
    % Todos los par�metros que se puedan variar durante la ejecuci�n del
    % programa, probablemente a trav�s de la GUI.
    properties(SetAccess = public)
    end
    
    
    %% Propiedades privadas
    properties(SetAccess = private)
    end
    
    
    %% M�todos p�blicos
    methods
        
        
        function obj = PreprocesadorBlur(nombre, varargin)
            % Constructor
            
            % Validaci�n de entradas
            % ToDo
            
            % Constructor de superclase
            obj@Preprocesador(nombre);
            
            
            % Par�metros
            % Par�metros variables de esta etapa. Son miembros de la
            % estructura p. Su formato:
            % Parametro(por_defecto, min, max, valor);
            
            %   *sigma*: Desviaci�n est�ndar de la distribuci�n gaussiana
            %   del kernel de filtrado, en p�xeles.
            %       0 <= sigma < inf
            obj.p.sigma = Parametro(double(2), 0, 50, 2);
            
        end
        
    end
    
    
    %% M�todos protegidos
    methods(Access = protected)
        
        
        function [img_frente, img_dorso, internos] = procesarResultados(obj, Resultados)
            %PROCESARRESULTADOS Procesa un objeto de clase Resultados
            % Uso: [img_frente, img_dorso, internos] = procesarResultados(obj, resultados)
            % *img_frente*: imagen procesada.
            % *img_dorso*: imagen procesada.
            % *internos*: Estructura de informaci�n que emite la etapa.
            %   Por ejemplo, valores que midi�, o im�genes �tiles
            %   para depuraci�n.
            % *resultados*: Objeto de la clase Resultados.
            
            % Validaci�n de entradas
            % ToDo
            
            % Estructura de resultados internos.
            internos = struct();
            
            % Procesamiento
            img_frente = obj.procesarImagen(Resultados.imagen_frente);
            img_dorso = obj.procesarImagen(Resultados.imagen_dorso);
            
        end
        
        
    end % methods protected
    
    
    %% M�todos privados
    methods(Access = private)
        
        
        function filtrada = procesarImagen(obj, i_img)
            % PROCESARIMAGEN
            % Uso: filtrada = procesarImagen(obj, i_img)
            % *filtrada*: imagen suavizada.
            % *i_img*: imagen rgb a suavizar con filtro gaussiano.
            
            % Validaci�n de entradas
            %- La imagen es a color, 3 canales: RGB
            imagen = i_img;
            validateattributes(imagen, {'uint8'}, {'nonempty', 'ndims', 3, 'size', [NaN, NaN, 3]}, ...
                'segmentar', 'imagen', 1);
            
            % Blur
            % Comentario: If you do not specify the 'Padding' parameter, imgaussfilt uses
            % 'replicate' padding by default, which is different from the default used by imfilter.
            filtrada = imgaussfilt(i_img, obj.p.sigma.valor);
            
        end
        
        
    end % Methods private
    
    
end % classdef