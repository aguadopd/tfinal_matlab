classdef ClasificadorSolidez < Clasificador
    %CLASIFICADORSOLIDEZ [ALMENDRAS] Solidez
    %
    % Este clasificador analiza la solidez del objeto, la cu�l se define
    % como la relaci�n entre el �rea real y el �rea de la envolvente
    % convexa (convex hull).
    %
    % Apunta a detectar almendras astilladas.
    %
    % La solidez medida es la m�nima entre las calculadas para el frente y
    % el dorso.
    %
    % Etiqueta seg�n par�metros variables. Si se encuentra fuera de
    % un cierto rango, se considera Mala.
    %
    % PARAMETROS
    %   *umbral_min*: Solidez m�nima para ser considerada Primera.
    %       0 <= umbral_min < umbral_max
    %   *umbral_max*: Solidez m�xima para ser considerado Primera
    %       umbral_min < umbral_max < 1
    
    
    %% Propiedades p�blicas
    % Todos los par�metros que se puedan variar durante la ejecuci�n del
    % programa, probablemente a trav�s de la GUI.
    properties(SetAccess = public)
    end
    
    
    %% Propiedades privadas
    properties(SetAccess = private)
    end
    
    
    %% M�todos p�blicos
    methods(Access = public)
        
        
        function obj = ClasificadorSolidez(nombre, varargin)
            % Constructor
            
            % Validaci�n de entradas
            % ToDo
            
            % Constructor de superclase
            obj@Clasificador(nombre);
            
            % Par�metros
            % Par�metros variables de esta etapa. Son miembros de la
            % estructura p. Su formato:
            % Parametro(por_defecto, min, max, valor);
            
            % *umbral_min*: Altura m�nima para ser considerado almendra. En
            % p�xeles.
            obj.p.umbral_min = Parametro(double(0.99), 0, 11, 0.99);
            
            % *umbral_max*: Altura m�xima para ser considerado almendra. En
            % p�xeles.
            obj.p.umbral_max = Parametro(double(1), 0, 1, 1);            
            
        end
        
        
    end
    
    
    %% M�todos protegidos
    methods(Access = protected)
        
        
        function [clase, internos] = clasificarResultados(obj, resultados)
            %CLASIFICARRESULTADOS Clasifica un Resultado
            % Uso: [clase, internos] = clasificarResultados(obj, resultados)
            % *clase*: Objeto de la clase Clases. Etiqueta.
            % *internos*: Estructura de informaci�n que emite la etapa
            %   clasificadora. Por ejemplo, valores que midi�, o im�genes �tiles
            %   para depuraci�n. En uso actualmente: campo 'valor', es la
            %   medici�n m�s importante que realiza la etapa. El descriptor.
            % *resultados*: Objeto de la clase Resultados
            
            % Estructura de resultados internos.
            internos = struct();
            
            % Clasificaci�n de frente y dorso
            [solidez_frente, internos.imagen_frente] = ...
                obj.clasificarImagen(resultados.mascara_frente);
            [solidez_dorso, internos.imagen_dorso] = ...
                obj.clasificarImagen(resultados.mascara_dorso);
            
            % La solidez del objeto es la m�nima de las calculadas
            % en la imagen de frente y la de dorso.
            solidez = min([solidez_frente, solidez_dorso]);
            
            % Se guarda el valor en la estructura de resultados internos.
            internos.valor = solidez;
            
            % Etiqueta seg�n par�metros variables. Si se encuentra fuera de
            % un cierto rango, se considera Mala.
            if solidez < obj.p.umbral_min.valor || solidez > obj.p.umbral_max.valor
                clase = Clases.Mala;
            else
                clase = Clases.Primera;
            end
            
        end
        
        
    end % methods protected
    
    
    %% M�todos privados
    methods(Access = private)
        
        
        function [solidez, img] = clasificarImagen(obj, mascara)
            %CLASIFICARIMAGEN Clasifica una imagen de m�scara
            % Uso: [solidez, img] = clasificarImagen(obj, i_mascara)
            % *solidez*: area/area_convex_hull
            % *img*: Imagen auxiliar �til para depuraci�n. No implementada.
            
            
            % Validaci�n de entradas
            % La mascara es bidimensional y l�gica
            validateattributes(mascara, {'logical'}, {'nonempty', 'ndims', 2}, ...
                'clasificarImagen', 'i_mascara', 1);
            
            
            % Propiedades de las regiones conectadas.
            propiedades = regionprops(mascara, 'Solidity');
            
            % Suponemos que s�lo hay una regi�n conectada ---solamente 1
            % objeto. Por las dudas, usamos la primera.
            solidez = propiedades(1).Solidity;
            
            
            % Aux
            img = [];
            
        end % clasificarImagen
        
        
    end % Methods private
    
    
end