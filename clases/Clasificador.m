classdef Clasificador
    %CLASIFICADOR [ALMENDRAS] Superclase para los clasificadores
    % Todo lo que entra al algoritmo de clasificaci�n se supone una
    % almendra de primera clase (Primera). Cada etapa de clasificaci�n
    % puede ir degradando esta clase, pero nunca subiendo. Las etapas de
    % clasificaci�n son independientes entre s�.
    
    
    %% Propiedades p�blicas
    % Todos los par�metros que se puedan variar durante la ejecuci�n del
    % programa, probablemente a trav�s de la GUI.
    properties(SetAccess = public)
        % Par�metros de cada clasificador. 'p' para brevedad. Es una
        % estructura de objetos de la clase Parametro.
        p = struct();
    end
    
    
    %% Propiedades privadas
    properties(SetAccess = protected)
        % Nombre del objeto creado, para m�s f�cil identificaci�n.
        nombre_objeto = '';
    end
    
    
    %% M�todos p�blicos
    methods
        
        
        function obj = Clasificador(nombre, varargin)
            % Constructor
            
            % Validaci�n de entradas
            % ToDo
            
            % Creaci�n
            obj.nombre_objeto = nombre;
        end
        
        
        function resultados = clasificar(obj, resultados, flag_forzar)
            %CLASIFICAR. Rellena objeto resultados
            % Uso: Resultados = clasificar(obj, resultados, flag_forzar)
            % *resultados* = Objeto de la clase Resultados
            % *flag_forzar* = Booleano para forzar o no clasificaci�n.
            %
            % flag_forzar = false hace que no se eval�en aquellos
            % objetos que ya se consideran de clase Clases.Nada,
            % Clases.Mala o Clases.NoAlmendra. Se usa para un sistema en
            % l�nea, que no derrocha tiempo si ya se tom� una decisi�n.
            %
            % flag_forzar = true fuerza la clasificaci�n del objeto, y
            % sirve para obtener todas las mediciones de todos los objetos.
            
            % Validaci�n de entradas
            % ToDo
            
            
            % Clasificaci�n
            %   Forzada:
            if flag_forzar == true
                clase_anterior = resultados.clase;
                
                % Clasificaci�n
                [clase, internos] = clasificarResultados(obj, resultados);
                
                % Se guarda un registro escrito de la decisi�n tomada, en
                % la propiedad log.
                resultados.log{end+1} = ...
                    sprintf('Clase %s seg�n clasificador %s', char(clase), obj.nombre_objeto);
                resultados.log{end+1} = sprintf('\t\t%f', internos.valor);
                
                % Se guarda la etapa que rechaz� primero. �til para
                % depuraci�n en tablas.
                if clase < clase_anterior
                    resultados.causa_rechazo = class(obj);
                end
                
                % Forzamos el no ascenso de clase.
                if clase > clase_anterior
                    clase = clase_anterior;
                end
                
                resultados.clase = clase;
                
                
            else
                %   No forzada:
                % Si todav�a se considera almendra, se eval�a.
                if resultados.clase ~= Clases.Nada && ...
                        resultados.clase ~= Clases.Mala && ...
                        resultados.clase ~= Clases.NoAlmendra
                    
                    clase_anterior = resultados.clase;
                    
                    % Clasificaci�n
                    [clase, internos] = clasificarResultados(obj, resultados);
                    
                    % Se guarda un registro escrito de la decisi�n tomada, en
                    % la propiedad log.
                    resultados.log{end+1} = ...
                        sprintf('Clase %s seg�n clasificador %s', char(clase), obj.nombre_objeto);
                    resultados.log{end+1} = sprintf('\t\t%f', internos.valor);
                    
                     % Se guarda la etapa que rechaz� primero. �til para
                     % depuraci�n en tablas.
                    if clase < clase_anterior
                        resultados.causa_rechazo = class(obj);
                    end
                    
                    % Forzamos el no ascenso de clase.
                    if clase > clase_anterior
                        clase = clase_anterior;
                    end
                    
                    resultados.clase = clase;
                    
                else
                    % Se crea la estructura de resultados internos
                    internos = struct('evaluado', false);
                end
            end
            
            % Se completa y guarda la estructura de metainformaci�n de esta
            % etapa.
            internos.nombre_objeto = obj.nombre_objeto;
            resultados.internos{end+1} = internos;
            
        end % clasificar
        
        
    end % methods
    
    
    %% M�todos abstractos
    % A implementar por cada etapa clasificadora
    methods(Abstract = true, Access = protected)
        
        %CLASIFICARRESULTADOS Clasifica un Resultado
        % Uso: [clase, internos] = clasificarResultados(obj, Resultados)
        % *clase*: Objeto de la clase Clases. Etiqueta.
        % *internos*: Estructura de informaci�n que emite la etapa
        %   clasificadora. Por ejemplo, valores que midi�, o im�genes �tiles
        %   para depuraci�n. En uso actualmente: campo 'valor', es la
        %   medici�n m�s importante que realiza la etapa. El descriptor.
        % *resultados*: Objeto de la clase Resultados.
        [clase, internos] = clasificarResultados(obj, resultados)
        
        
    end % methods ( Abstract = true )
    
    
end % classdef