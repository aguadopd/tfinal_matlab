classdef Segmentador_tests < matlab.unittest.TestCase
    %SEGMENTAR_TEST Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (ClassSetupParameter)
        %tols = struct('t1', 1, 't2', 2, 't5', 5, 't10', 10, 't20', 20); 
        tol = {'0.5','1','2','5','10'}; % en porcentaje
        
    end
    
    properties
        tolerancia @double
        ruta @char
        S @Segmentador
        guardar @logical
    end
   
    
    %%
    % Configuración que se ejecuta antes de empezar las pruebas (y su
    % limpieza)
    methods(TestClassSetup)
        
        function setupAgregarCarpeta( testCase )
            %global g_carpeta_para_tests;
            p = path;
            testCase.addTeardown(@path,p);
            addpath(testCase.ruta);
        end
        
        function setupConfiguracion( testCase, tol)
            testCase.tolerancia = str2double(tol)/100;
        end
       
    end
    
    %%
    % Configuración antes de cada prueba (y su limpieza)
    methods(TestMethodSetup)
    end
    
    %% Constructor con configuración
    methods
       
        function testCase = Segmentador_tests( i_ruta, i_guardar )
            testCase.ruta = i_ruta;
            testCase.S = Segmentador(4,2);
            testCase.guardar = logical(i_guardar);
        end
        
    end
    
    %% TESTS
    methods(Test)
        
        % 0. Prueba
        function test0AlmendraCompleta( testCase )
            archivo_original = '02_completa.png';
            archivo_mascara_manual = '02_completa_m.png';
            [area_real, area_esperada] = proceso(testCase, archivo_original, archivo_mascara_manual);            
            testCase.verifyEqual(area_real,area_esperada,'RelTol',testCase.tolerancia); 
        end
        
        
        % 1. 1 almendra completa.
        function test1AlmendraCompleta( testCase )
            archivo_original = '01_completa.png';
            archivo_mascara_manual = '01_completa_m.png';
            [area_real, area_esperada] = proceso(testCase, archivo_original, archivo_mascara_manual);            
            testCase.verifyEqual(area_real,area_esperada,'RelTol',testCase.tolerancia); 
        end
        
        
        % 2. 1 almendra sin tegumento en el centro.
        function test2AlmendraSinTegumentoCentro( testCase )
            archivo_original = '1_b.png';
            archivo_mascara_manual = '1_b_manual.png';
            [area_real, area_esperada] = proceso(testCase, archivo_original, archivo_mascara_manual);            
            testCase.verifyEqual(area_real,area_esperada,'RelTol',testCase.tolerancia); 
        end
        
        
        % 3. 1 almendra sin tegumento en el borde.
        function test3AlmendraSinTegumentoBorde( testCase )
            archivo_original = '1_b.png';
            archivo_mascara_manual = '1_b_manual.png';
            [area_real, area_esperada] = proceso(testCase, archivo_original, archivo_mascara_manual);            
            testCase.verifyEqual(area_real,area_esperada,'RelTol',testCase.tolerancia); 
        end
        
        
        % 4. 1 almendra a la que le falte menos de 1/2 de la semilla.
        function test4AlmendraIncompleta( testCase )
            archivo_original = '1_b.png';
            archivo_mascara_manual = '1_b_manual.png';
            [area_real, area_esperada] = proceso(testCase, archivo_original, archivo_mascara_manual);            
            testCase.verifyEqual(area_real,area_esperada,'RelTol',testCase.tolerancia); 
        end
        
        
        % 5. 1 mitad de almendra, viendo el interior.
        function test5AlmendraMitadInterior( testCase )
            archivo_original = '05_mitad.png';
            archivo_mascara_manual = '05_mitad_m.png';
            [area_real, area_esperada] = proceso(testCase, archivo_original, archivo_mascara_manual);            
            testCase.verifyEqual(area_real,area_esperada,'RelTol',testCase.tolerancia); 
        end
        
        
        % 6. 1 pedazo de almendra de 3 mm de diámetro aproximadamente.
        function test6AlmendraTrozo3mm( testCase )
            archivo_original = '1_b.png';
            archivo_mascara_manual = '1_b_manual.png';
            [area_real, area_esperada] = proceso(testCase, archivo_original, archivo_mascara_manual);            
            testCase.verifyEqual(area_real,area_esperada,'RelTol',testCase.tolerancia); 
        end
        
        
        % 7. 1 almendra con endocarpio.
        function test7Endocarpio( testCase )
            archivo_original = '04_endocarpio.png';
            archivo_mascara_manual = '04_endocarpio_m.png';
            [area_real, area_esperada] = proceso(testCase, archivo_original, archivo_mascara_manual);            
            testCase.verifyEqual(area_real,area_esperada,'RelTol',testCase.tolerancia); 
        end
        
        
        % 8. 1 pedazo de endocarpio mayor a 3 mm de diámetro.
        function test8EndocarpioTrozo( testCase )
            archivo_original = '03_cascara.png';
            archivo_mascara_manual = '03_cascara_m.png';
            [area_real, area_esperada] = proceso(testCase, archivo_original, archivo_mascara_manual);            
            testCase.verifyEqual(area_real,area_esperada,'RelTol',testCase.tolerancia); 
        end
        
        
        % 9. 1 pedazo de vidrio mayor a 3 mm de diámetro.
        function test9VidrioTrozo( testCase )
            archivo_original = '1_b.png';
            archivo_mascara_manual = '1_b_manual.png';
            [area_real, area_esperada] = proceso(testCase, archivo_original, archivo_mascara_manual);            
            testCase.verifyEqual(area_real,area_esperada,'RelTol',testCase.tolerancia); 
        end
        
        
        % 10. 1 pedazo de plástico mayor a 3 mm de diámetro.
        function test10PlasticoTrozo( testCase )
            archivo_original = '1_b.png';
            archivo_mascara_manual = '1_b_manual.png';
            [area_real, area_esperada] = proceso( testCase, archivo_original, archivo_mascara_manual);            
            testCase.verifyEqual(area_real,area_esperada,'RelTol',testCase.tolerancia); 
        end
        
        
        % 11. 1 ramita con un grosor mayor a 3 mm.
        function test11Rama( testCase )
            archivo_original = '06_rama.png';
            archivo_mascara_manual = '06_rama_m.png';
            [area_real, area_esperada] = proceso(testCase, archivo_original, archivo_mascara_manual);            
            testCase.verifyEqual(area_real,area_esperada,'RelTol',testCase.tolerancia); 
        end
        
        
    end
    
end






function [area_real, area_esperada] = proceso(testCase, archivo_original, archivo_mascara_manual)
    rgb = imread(archivo_original);
    mascara_manual = im2bw(imread(archivo_mascara_manual));
    mascara_objeto = testCase.S.segmentar1(rgb);
    if testCase.guardar
        imwrite(imfuse(mascara_manual,mascara_objeto,'falsecolor'), ...
            fullfile(pwd,'temp',archivo_original),'png');
    end
    area_real = nnz(mascara_objeto);
    area_esperada = nnz(mascara_manual);
end